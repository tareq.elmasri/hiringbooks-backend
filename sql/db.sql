-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jun 25, 2016 at 10:48 AM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `bookworm`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity`
--

CREATE TABLE `activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_name` int(11) NOT NULL,
  `activity` text NOT NULL,
  `topic_id` int(11) NOT NULL,
  `topic_type` varchar(10) NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `activity`
--

INSERT INTO `activity` (`id`, `user_id`, `user_name`, `activity`, `topic_id`, `topic_type`, `seen`, `timestamp`) VALUES
(3, 4, 0, 'has posted a book.', 41, 'books', 0, '2016-06-04T20:57:42.739Z'),
(4, 4, 0, 'has posted a book.', 42, 'books', 0, '2016-06-04T20:58:21.180Z'),
(5, 4, 0, 'has posted a book.', 43, 'books', 0, '2016-06-04T20:59:11.065Z'),
(6, 4, 0, 'has posted a book.', 44, 'books', 0, '2016-06-04T20:59:59.633Z'),
(7, 4, 0, 'has posted a book.', 45, 'books', 0, '2016-06-04T21:02:45.676Z'),
(8, 4, 0, 'has requested a book.', 14, 'requests', 0, '2016-06-04T21:43:29.307Z');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE `books` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 NOT NULL,
  `author` text NOT NULL,
  `user_id` int(100) NOT NULL,
  `image_path` varchar(150) NOT NULL,
  `rating` int(11) NOT NULL,
  `ratings_count` int(11) NOT NULL,
  `goodreads_id` int(11) NOT NULL,
  `isbn` varchar(30) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `comments_count` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `user_id`, `image_path`, `rating`, `ratings_count`, `goodreads_id`, `isbn`, `timestamp`, `status`, `comments_count`, `price`) VALUES
(4, 'La Hobito, a?, Tien kaj Reen', 'J.R.R. Tolkien', 1, 'http://d.gr-assets.com/books/1445528215m/27255844.jpg', 4, 1927636, 1540236, '', 1464318577, 1, 0, 0),
(5, 'Animal Farm', 'Daniel Moran', 1, 'http://d.gr-assets.com/books/1360056318m/7616.jpg', 4, 46, 7616, '', 1464319063, 1, 1, 0),
(6, 'Fifty Shades of Grey (Fifty Shades, #1)', 'E.L. James', 1, 'http://d.gr-assets.com/books/1385207843m/10818853.jpg', 4, 1316541, 10818853, '', 1464324841, 1, 0, 0),
(7, 'Throne of the Crescent Moon (The Crescent Moon Kingdoms, #1)', 'Saladin Ahmed', 1, 'http://d.gr-assets.com/books/1344256189m/11487807.jpg', 4, 7256, 11487807, '', 1464324986, 1, 8, 0),
(9, 'The Forbidden Game (The Forbidden Game, #1-3)', 'L.J. Smith', 1, 'http://d.gr-assets.com/books/1405784030m/7100490.jpg', 4, 18321, 7100490, '', 1464344479, 0, 0, 0),
(10, 'Animal Farm / 1984', 'George Orwell', 1, 'http://d.gr-assets.com/books/1327959366m/5472.jpg', 4, 88931, 5472, '', 1464352646, 1, 1, 0),
(12, 'A Game of Thrones (A Song of Ice and Fire, #1)', 'George R.R. Martin', 1, 'http://d.gr-assets.com/books/1436732693m/13496.jpg', 4, 1187495, 13496, '', 1464381011, 1, 4, 0),
(13, 'Animal Farm', 'George Orwell', 1, 'http://d.gr-assets.com/books/1424037542m/7613.jpg', 4, 1741904, 7613, '', 1464381242, 1, 113, 0),
(14, 'Everything Is Illuminated', 'Jonathan Safran Foer', 4, 'http://d.gr-assets.com/books/1327865538m/256566.jpg', 4, 131236, 256566, '', 1464962596, 1, 0, 0),
(15, 'The Book Thief', 'Markus Zusak', 4, 'http://d.gr-assets.com/books/1390053681m/19063.jpg', 4, 1051462, 19063, '', 1464962658, 1, 0, 20),
(16, 'Rela Kumenanti', 'Damya Hanna', 4, 'http://d.gr-assets.com/books/1338877801m/6991132.jpg', 4, 202, 6991132, '', 2016, 1, 0, 0),
(17, 'Relativity: The Special and the General Theory', 'Albert Einstein', 4, 'http://d.gr-assets.com/books/1420607613m/15852.jpg', 4, 11524, 15852, '', 2016, 1, 1, 300),
(18, 'Animal Farm', 'George Orwell', 4, 'http://d.gr-assets.com/books/1424037542m/7613.jpg', 4, 1746324, 7613, '', 2016, 1, 0, 20),
(19, 'Animal Farm', 'Ian Wooldridge', 4, 'http://d.gr-assets.com/books/1328704319m/253305.jpg', 4, 327, 253305, '', 2016, 1, 0, 0),
(20, 'Fables, Vol. 2: Animal Farm', 'Bill Willingham', 4, 'http://d.gr-assets.com/books/1364233122m/167010.jpg', 4, 21906, 167010, '', 2016, 1, 0, 0),
(21, 'Animal Farm and Related Readings', 'McDougal Littell', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 1231, 3341119, '', 2016, 1, 0, 0),
(22, 'Animal Farm', 'Lona MacGregor', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 34, 1013854, '', 2016, 1, 0, 0),
(23, 'Animal Farm By George Orwell', 'Jean Armstrong', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 44, 465454, '', 2016, 1, 0, 0),
(24, 'Baby Animal Farm', 'Karen Blair', 4, 'http://d.gr-assets.com/books/1384018296m/18209424.jpg', 4, 52, 18209424, '', 2016, 1, 0, 0),
(25, 'Animal Farm (SparkNotes Literature Guide)', 'SparkNotes', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 18, 52053, '', 2016, 1, 0, 0),
(26, 'The Animal Farm', 'Rolando Merino', 4, 'http://d.gr-assets.com/books/1388161066m/20321546.jpg', 3, 6, 20321546, '', 2016, 1, 0, 0),
(27, 'Our Farm: By the Animals of Farm Sanctuary', 'Maya Gottfried', 4, 'http://d.gr-assets.com/books/1320499058m/6819632.jpg', 4, 62, 6819632, '', 2016, 1, 0, 0),
(28, 'George Orwell Omnibus: The Complete Novels: Animal Farm, Burmese Days, A Clergyman''s Daughter, Coming up for Air, Keep the Aspidistra Flying, and Nineteen Eighty-Four', 'George Orwell', 4, 'http://d.gr-assets.com/books/1374680528m/862873.jpg', 4, 527, 862873, '', 2016, 1, 0, 0),
(29, 'George Orwell''s Animal Farm', 'Richard Peaslee', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 16, 12320443, '', 2016, 1, 0, 0),
(30, 'Animal Farm', 'Tony Rubino', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 3, 17275858, '', 2016, 1, 0, 0),
(31, 'Animal Farm', 'Daniel Naude', 4, 'http://d.gr-assets.com/books/1420785636m/15917025.jpg', 5, 1, 15917025, '', 2016, 1, 0, 0),
(32, 'Animal farm: By George Orwell (Teacher''s companion : a resource guide for teachers, by teachers)', 'Robert W. Menchhofer', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 159, 1109848, '', 2016, 1, 0, 0),
(33, 'Project Animal Farm: An Accidental Journey into the Secret World of Farming and the Truth About Our Food', 'Sonia Faruqi', 4, 'http://d.gr-assets.com/books/1424793303m/25010358.jpg', 4, 113, 25010358, '', 2016, 1, 0, 0),
(34, 'Animal Farm, George Orwell: Guide', 'John Mahoney', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 22, 2561315, '', 2016, 1, 0, 0),
(35, 'Animal Farm', 'Ian  Smith', 4, 'http://d.gr-assets.com/books/1341499826m/15737120.jpg', 3, 1, 15737120, '', 2016, 1, 0, 0),
(36, 'Animal Farm', 'Daniel Moran', 4, 'http://d.gr-assets.com/books/1360056318m/7616.jpg', 4, 46, 7616, '', 2016, 1, 0, 0),
(37, 'Emma: A Romance Classic By Jane Austen! AAA+++', 'Jane Austen', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 452446, 20879048, '', 2016, 1, 0, 0),
(38, 'AAA, Vol. 01', 'Haruka Fukushima', 4, 'http://d.gr-assets.com/books/1371071032m/7554754.jpg', 4, 333, 7554754, '', 2016, 1, 0, 0),
(39, 'the one you''ve been waiting for! The Polar Express: A Teacher''s Guide (Activities and Lesson Plans)--just list! AAA+++', 'Chris Van Allsburg', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 134100, 22744345, '', 2016, 1, 0, 0),
(40, 'AAA, Vol. 03', 'Haruka Fukushima', 4, 'http://d.gr-assets.com/books/1371071133m/9727038.jpg', 4, 157, 9727038, '', 2016, 1, 0, 0),
(41, 'Forrest.Gump -ultimate Collection-AAA+++', 'Winston Groom', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 43053, 18979692, '', 2016, 1, 0, 0),
(42, 'Aaa!', 'Aldo Busi', 4, 'http://d.gr-assets.com/books/1363213284m/10139926.jpg', 4, 4, 10139926, '', 2016, 1, 0, 0),
(43, 'AAA, Vol. 01-04', 'Haruka Fukushima', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 71, 7769688, '', 2016, 1, 0, 0),
(44, 'Eight Cousins: A Classic Fiction Novel By Louisa May Alcott! AAA+++', 'Louisa May Alcott', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 26381, 20660138, '', 2016, 1, 0, 0),
(45, 'AAA CAA Alberta & British Columbia: Including Calgary, Edmonton, Vancouver, Victoria: Plus Driving D', 'AAA', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 0, 0, 28351077, '', 2016, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `comment` text NOT NULL,
  `parent_comment_id` int(12) NOT NULL,
  `timestamp` varchar(30) NOT NULL,
  `topic_id` varchar(30) NOT NULL,
  `topic_type` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=129 ;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `parent_comment_id`, `timestamp`, `topic_id`, `topic_type`, `user_id`, `target_user_id`) VALUES
(3, 'Hi', 0, '2016-05-27 20:51:40', '13', 'book', 1, 1),
(10, 'Hi', 0, '2016-05-27 21:07:33', '13', 'book', 1, 1),
(11, 'Hi', 0, '2016-05-27 21:09:00', '13', 'book', 1, 1),
(13, 'meow', 0, '2016-05-27 22:18:51', '13', 'book', 1, 1),
(14, ':relaxed:', 0, '2016-05-28 07:03:04', '13', 'book', 1, 1),
(15, ':blush:', 0, '2016-05-28 07:03:48', '13', 'book', 1, 1),
(16, ':star:', 0, '2016-05-28 08:05:45', '13', 'book', 1, 1),
(17, 'Hi', 0, '2016-05-28 08:19:40', '13', 'book', 1, 1),
(18, 'Heelo', 0, '2016-05-28 08:33:59', '13', 'book', 1, 1),
(19, ':star:', 0, '2016-05-28 11:01:19', '10', 'book', 1, 1),
(20, ':frog: :frog: :frog:', 0, '2016-05-28 14:48:39', '13', 'book', 1, 1),
(21, ':star:', 0, '2016-05-31 12:52:01', '5', 'book', 1, 1),
(22, 'Hello', 0, '2016-06-03 17:04:34', '13', 'book', 4, 1),
(23, 'Hello', 0, '2016-06-03 17:04:48', '13', 'book', 4, 1),
(24, 'Hello', 0, '2016-06-03 17:04:55', '13', 'book', 4, 1),
(25, 'Hello', 0, '2016-06-03 17:05:58', '13', 'book', 4, 1),
(26, 'Hello', 0, '2016-06-03 17:06:01', '13', 'book', 4, 1),
(27, 'Hello', 0, '2016-06-03 17:06:01', '13', 'book', 4, 1),
(28, 'Hello', 0, '2016-06-03 17:06:02', '13', 'book', 4, 1),
(29, 'Hello', 0, '2016-06-03 17:06:02', '13', 'book', 4, 1),
(30, 'Hello', 0, '2016-06-03 17:06:02', '13', 'book', 4, 1),
(31, 'aaa', 0, '2016-06-03 17:08:26', '13', 'book', 4, 1),
(32, 'Hi', 0, '2016-06-03 17:09:59', '13', 'book', 4, 1),
(33, 'Hi', 0, '2016-06-03 17:10:29', '13', 'book', 4, 1),
(34, 'Hi', 0, '2016-06-03 17:11:11', '13', 'book', 4, 1),
(35, 'Hi', 0, '2016-06-03 17:12:09', '13', 'book', 4, 1),
(36, 'Fuck', 0, '2016-06-03 17:12:44', '13', 'book', 4, 1),
(37, 'Fuck', 0, '2016-06-03 17:14:51', '13', 'book', 1, 1),
(38, 'Fuck', 0, '2016-06-03 17:15:44', '13', 'book', 4, 1),
(39, 'Fuck', 0, '2016-06-03 17:16:16', '13', 'book', 4, 1),
(40, 'Fuck', 0, '2016-06-03 17:16:29', '13', 'book', 4, 1),
(41, 'a', 0, '2016-06-03 17:17:35', '13', 'book', 4, 1),
(42, 'Fuck', 0, '2016-06-03T17:22:34.776Z', '13', 'book', 4, 1),
(43, 'Shit', 0, '2016-06-03T17:23:27.329Z', '13', 'book', 4, 1),
(44, 'Shit', 0, '2016-06-03T17:24:34.477Z', '13', 'book', 4, 1),
(45, 'Hello', 0, '2016-06-03T17:25:44.250Z', '13', 'book', 4, 1),
(46, 'Fuck', 0, '2016-06-03T17:28:09.223Z', '13', 'book', 4, 1),
(47, 'Fuck', 0, '2016-06-03T17:28:53.242Z', '13', 'book', 4, 1),
(48, 'Fuck', 0, '2016-06-03T17:29:21.773Z', '13', 'book', 4, 1),
(49, 'Shit', 0, '2016-06-03T17:29:31.307Z', '13', 'book', 4, 1),
(50, 'AAA', 0, '2016-06-03T17:30:18.817Z', '13', 'book', 4, 1),
(51, 'AAA', 0, '2016-06-03T17:31:07.095Z', '13', 'book', 4, 1),
(52, '3aaa', 0, '2016-06-03T17:31:14.493Z', '13', 'book', 4, 1),
(53, 'Shit', 0, '2016-06-03T17:32:02.874Z', '13', 'book', 4, 1),
(54, 'Fuck', 0, '2016-06-03T17:32:12.394Z', '13', 'book', 4, 1),
(55, 'aaa', 0, '2016-06-03T17:32:26.699Z', '13', 'book', 4, 1),
(56, 'aaa', 0, '2016-06-03T17:32:30.628Z', '13', 'book', 4, 1),
(57, 'Fuck', 0, '2016-06-03T17:34:49.474Z', '13', 'book', 4, 1),
(58, 'a7a', 0, '2016-06-03T17:35:00.532Z', '13', 'book', 4, 1),
(59, 'LOL', 0, '2016-06-03T17:35:21.129Z', '13', 'book', 4, 1),
(60, 'A', 0, '2016-06-03T17:35:45.374Z', '13', 'book', 4, 1),
(61, 'dada', 0, '2016-06-03T17:36:05.275Z', '13', 'book', 4, 1),
(62, 'jkkj', 0, '2016-06-03T17:38:05.154Z', '13', 'book', 4, 1),
(63, 'Hi', 0, '2016-06-03T17:40:05.946Z', '13', 'book', 4, 1),
(64, 'Hi', 0, '2016-06-03T17:41:23.157Z', '13', 'book', 4, 1),
(65, 'Hi', 0, '2016-06-03T17:44:54.703Z', '13', 'book', 4, 1),
(66, 'Fuck', 0, '2016-06-03T17:53:43.051Z', '13', 'book', 4, 1),
(67, 'Fuck', 0, '2016-06-03T17:55:33.495Z', '13', 'book', 4, 1),
(68, 'aaa', 0, '2016-06-03T17:56:18.292Z', '13', 'book', 4, 1),
(69, 'aada', 0, '2016-06-03T18:25:59.311Z', '13', 'book', 4, 1),
(70, 'Hi', 0, '2016-06-03T18:27:37.461Z', '13', 'book', 4, 1),
(71, 'Hi', 0, '2016-06-03T18:27:47.785Z', '13', 'book', 4, 1),
(72, 'Hi', 0, '2016-06-03T18:27:53.789Z', '13', 'book', 4, 1),
(73, 'Hi', 0, '2016-06-03T18:28:27.997Z', '13', 'book', 4, 1),
(74, 'Hi', 0, '2016-06-03T18:29:13.845Z', '13', 'book', 4, 1),
(75, 'Hi', 0, '2016-06-03T18:29:41.825Z', '13', 'book', 4, 1),
(76, 'a', 0, '2016-06-03T18:35:55.690Z', '13', 'book', 4, 1),
(77, 'Hello', 0, '2016-06-03T18:37:38.872Z', '13', 'book', 1, 1),
(78, 'Fuck', 0, '2016-06-03T18:37:51.251Z', '13', 'book', 1, 1),
(79, 'Haha', 0, '2016-06-03T18:50:01.987Z', '13', 'book', 4, 1),
(80, 'Hi', 0, '2016-06-03T18:55:47.488Z', '13', 'book', 4, 1),
(81, 'Hi', 0, '2016-06-03T19:02:41.776Z', '13', 'book', 4, 1),
(82, 'Hi', 0, '2016-06-03T19:02:58.514Z', '13', 'book', 4, 1),
(83, 'Hi', 0, '2016-06-03T19:03:08.532Z', '13', 'book', 4, 1),
(84, 'Hi', 0, '2016-06-03T19:03:24.919Z', '13', 'book', 4, 1),
(85, 'Hi', 0, '2016-06-03T19:04:16.745Z', '13', 'book', 4, 1),
(86, 'Hi', 0, '2016-06-03T19:04:29.977Z', '13', 'book', 4, 1),
(87, 'Hi', 0, '2016-06-03T19:05:44.188Z', '13', 'book', 4, 1),
(88, 'Hi', 0, '2016-06-03T19:05:54.108Z', '13', 'book', 4, 1),
(89, 'hh', 0, '2016-06-03T19:06:49.877Z', '13', 'book', 4, 1),
(90, 'Hi', 0, '2016-06-03T19:08:46.636Z', '13', 'book', 4, 1),
(91, 'Hi', 0, '2016-06-03T19:09:02.241Z', '13', 'book', 4, 1),
(92, 'Hi', 0, '2016-06-03T19:09:35.339Z', '13', 'book', 4, 1),
(93, 'Hi', 0, '2016-06-03T19:09:43.410Z', '13', 'book', 4, 1),
(94, 'Hi\\', 0, '2016-06-03T19:10:06.913Z', '13', 'book', 4, 1),
(95, 'j', 0, '2016-06-03T19:10:53.836Z', '13', 'book', 4, 1),
(96, 'jh', 0, '2016-06-03T19:11:29.440Z', '13', 'book', 4, 1),
(97, 'jjj', 0, '2016-06-03T19:11:37.440Z', '13', 'book', 4, 1),
(98, 'h', 0, '2016-06-03T19:11:58.253Z', '13', 'book', 4, 1),
(99, 'jkl', 0, '2016-06-03T19:14:13.025Z', '13', 'book', 4, 1),
(100, 'Test', 0, '2016-06-03T19:20:00.705Z', '13', 'book', 4, 1),
(101, 'a', 0, '2016-06-03T19:20:35.112Z', '13', 'book', 4, 1),
(102, 'Hello', 0, '2016-06-03T19:21:47.769Z', '13', 'book', 4, 1),
(103, 'Hello', 0, '2016-06-03T19:22:25.785Z', '13', 'book', 4, 1),
(104, 'Hi', 0, '2016-06-03T19:28:21.274Z', '13', 'book', 4, 1),
(105, 'hi', 0, '2016-06-03T19:29:10.518Z', '13', 'book', 4, 1),
(106, 'Fuck', 0, '2016-06-03T19:31:56.516Z', '13', 'book', 4, 1),
(107, 'Hi', 0, '2016-06-03T19:32:36.120Z', '13', 'book', 4, 1),
(108, 'l', 0, '2016-06-03T19:33:17.290Z', '13', 'book', 4, 1),
(109, 'Hi', 0, '2016-06-03T19:34:13.928Z', '13', 'book', 4, 1),
(110, 'Hi', 0, '2016-06-06T21:20:08.945Z', '1', 'wall', 4, 4),
(111, 'Holy Fuck :foggy:', 0, '2016-06-06T21:22:57.937Z', '1', 'wall', 4, 4),
(112, 'Hi', 0, '2016-06-21T15:38:57.023Z', '45', 'book', 4, 4),
(113, 'Hi', 0, '2016-06-21T15:41:22.646Z', '14', 'request', 4, 4),
(114, 'Hi', 0, '2016-06-21T15:42:46.729Z', '14', 'request', 4, 4),
(115, 'Hi', 0, '2016-06-21T15:43:25.958Z', '14', 'request', 4, 4),
(116, 'Hi', 0, '2016-06-21T15:45:03.715Z', '14', 'request', 4, 4),
(117, 'dadada', 0, '2016-06-21T15:48:27.502Z', '14', 'request', 4, 4),
(118, 'LOL', 0, '2016-06-24T20:05:35.712Z', '13', 'book', 1, 1),
(119, 'Hell yeah!', 0, '2016-06-24T20:34:35.586Z', '12', 'book', 3, 1),
(120, 'Hell yeah!', 0, '2016-06-24T20:40:59.350Z', '12', 'book', 3, 1),
(121, 'Hell yeah!', 0, '2016-06-24T20:43:03.009Z', '7', 'book', 3, 1),
(122, 'Hell yeah!', 0, '2016-06-24T21:18:47.057Z', '7', 'book', 3, 1),
(123, 'Hell yeah!', 0, '2016-06-24T21:20:44.059Z', '7', 'book', 3, 1),
(124, 'LOL', 0, '2016-06-24T21:20:52.233Z', '7', 'book', 3, 1),
(125, 'LOL', 0, '2016-06-24T21:22:14.570Z', '7', 'book', 3, 1),
(126, 'LOL', 0, '2016-06-24T21:55:20.234Z', '7', 'book', 3, 1),
(127, 'jj', 0, '2016-06-24T21:56:16.607Z', '7', 'book', 3, 1),
(128, 'jjiioiouoiuo', 0, '2016-06-24T21:56:53.882Z', '7', 'book', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` varchar(150) NOT NULL,
  `message` text NOT NULL,
  `target_id` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `timestamp`, `message`, `target_id`, `user_id`, `seen`) VALUES
(1, '2016-05-27T11:36:47.651Z', 'a', '1', 1, 1),
(2, '2016-05-27T11:36:59.196Z', 'Okay', '1', 1, 1),
(3, '2016-05-27T11:38:39.102Z', 'da', '1', 1, 1),
(4, '2016-05-27T11:39:12.488Z', 'dasda', '1', 1, 1),
(5, '2016-05-27T11:42:55.472Z', 'a', '1', 1, 1),
(6, '2016-05-27T11:42:57.291Z', 'dasda', '1', 1, 1),
(7, '2016-05-27T11:47:21.298Z', 'dada', '1', 1, 1),
(8, '2016-05-27T11:47:22.273Z', 'dadasdas', '1', 1, 1),
(9, '2016-05-27T11:47:23.226Z', 'dadasdd', '1', 1, 1),
(10, '2016-05-27T11:47:23.930Z', 'asdasdas', '1', 1, 1),
(11, '2016-05-27T11:47:24.491Z', 'dasdasd', '1', 1, 1),
(12, '2016-05-27T11:47:24.954Z', 'asd', '1', 1, 1),
(13, '2016-05-27T11:47:34.196Z', 'dasdas', '1', 1, 1),
(14, '2016-05-27T11:47:39.410Z', 'dadadsd', '1', 1, 1),
(15, '2016-05-27T11:47:39.955Z', 'adasdd', '1', 1, 1),
(16, '2016-05-27T11:47:40.195Z', 'asd', '1', 1, 1),
(17, '2016-05-27T11:47:40.362Z', 'asd', '1', 1, 1),
(18, '2016-05-27T11:47:41.251Z', 'ads', '1', 1, 1),
(19, '2016-05-28T07:06:34.482Z', ':relaxed:', '1', 1, 1),
(20, '2016-05-28T07:06:41.346Z', ':relaxed: :relaxed: :relaxed: :blush:', '1', 1, 1),
(21, '2016-05-28T14:53:09.495Z', ':skull: :skull:', '1', 1, 1),
(22, '2016-06-03T17:58:07.220Z', 'Hi', '1', 4, 1),
(23, '2016-06-03T17:58:34.963Z', 'Hi', '1', 4, 1),
(24, '2016-06-03T18:04:59.806Z', 'a', '1', 1, 1),
(25, '2016-06-03T18:05:10.814Z', 'a', '1', 1, 1),
(26, '2016-06-03T18:05:15.951Z', 'a', '4', 1, 1),
(27, '2016-06-03T18:05:16.967Z', 'a', '4', 1, 1),
(28, '2016-06-03T18:05:18.639Z', 'a', '4', 1, 1),
(29, '2016-06-03T18:21:33.767Z', 'a', '1', 4, 1),
(30, '2016-06-03T18:21:39.191Z', 'adasda', '1', 4, 1),
(31, '2016-06-03T18:22:39.411Z', 'a', '4', 1, 1),
(32, '2016-06-03T18:24:24.817Z', 'a', '1', 4, 1),
(33, '2016-06-03T18:24:31.161Z', 'dadas', '1', 4, 1),
(34, '2016-06-21T15:55:17.592Z', 'Hi', '3', 1, 1),
(35, '2016-06-21T15:55:22.079Z', 'Hi', '4', 1, 1),
(36, '2016-06-21T15:55:34.071Z', 'Hi', '4', 1, 1),
(37, '2016-06-21T15:55:53.424Z', 'Hi', '1', 4, 1),
(38, '2016-06-21T15:56:08.346Z', 'Hi', '1', 4, 1),
(39, '2016-06-21T15:56:10.608Z', 'Hi', '1', 4, 1),
(40, '2016-06-21T15:56:11.384Z', 'Hi', '1', 4, 1),
(41, '2016-06-21T16:00:52.809Z', 'Hi', '1', 4, 0),
(42, '2016-06-25T08:33:12.503Z', ':blush: :blush:', '1', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `notification` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `seen` tinyint(1) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `topic_type` varchar(30) NOT NULL,
  `timestamp` varchar(50) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `name`, `notification`, `user_id`, `target_user_id`, `seen`, `topic_id`, `topic_type`, `timestamp`) VALUES
(20, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T17:53:43.051Z'),
(21, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T17:55:33.495Z'),
(22, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T17:56:18.292Z'),
(23, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:25:59.311Z'),
(24, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:27:37.461Z'),
(25, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:27:47.785Z'),
(26, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:27:53.789Z'),
(27, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:28:27.997Z'),
(28, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T18:29:13.845Z'),
(29, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T18:29:41.825Z'),
(30, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T18:35:55.690Z'),
(31, 'Yiara Omara', 'commented on your book.', 1, 1, 0, 13, 'books', '2016-06-03T18:37:38.872Z'),
(32, 'Yiara Omara', 'commented on your book.', 1, 1, 0, 13, 'books', '2016-06-03T18:37:51.251Z'),
(33, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T18:50:01.987Z'),
(34, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T18:55:47.488Z'),
(35, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:02:41.776Z'),
(36, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:02:58.514Z'),
(37, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:03:08.532Z'),
(38, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:03:24.919Z'),
(39, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:04:16.745Z'),
(40, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:04:29.977Z'),
(41, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:05:44.188Z'),
(42, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:05:54.108Z'),
(43, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:06:49.877Z'),
(44, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:08:46.636Z'),
(45, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:09:02.241Z'),
(46, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:09:35.339Z'),
(47, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:09:43.410Z'),
(48, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:10:06.913Z'),
(49, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:10:53.836Z'),
(50, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:11:29.440Z'),
(51, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:11:37.440Z'),
(52, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:11:58.253Z'),
(53, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:14:13.025Z'),
(54, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:20:00.705Z'),
(55, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:20:35.112Z'),
(56, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:21:47.769Z'),
(57, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:22:25.785Z'),
(58, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:28:21.274Z'),
(59, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:29:10.518Z'),
(60, 'Tareq El-Masri', 'commented on your book.', 4, 1, 0, 13, 'books', '2016-06-03T19:31:56.516Z'),
(61, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T19:32:36.120Z'),
(62, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T19:33:17.290Z'),
(63, 'Tareq El-Masri', 'commented on your book.', 4, 1, 1, 13, 'books', '2016-06-03T19:34:13.928Z'),
(64, 'Tareq El-Masri', 'commented on your book.', 4, 4, 0, 45, 'books', '2016-06-21T15:38:57.023Z'),
(65, 'Tareq El-Masri', 'commented on your request.', 4, 4, 0, 14, 'requests', '2016-06-21T15:41:22.646Z'),
(66, 'Tareq El-Masri', 'commented on your request.', 4, 4, 0, 14, 'requests', '2016-06-21T15:42:46.729Z'),
(67, 'Tareq El-Masri', 'commented on your request.', 4, 4, 0, 14, 'requests', '2016-06-21T15:43:25.958Z'),
(68, 'Tareq El-Masri', 'commented on your request.', 4, 4, 0, 14, 'requests', '2016-06-21T15:45:03.715Z'),
(69, 'Tareq El-Masri', 'commented on your request.', 4, 4, 0, 14, 'requests', '2016-06-21T15:48:27.502Z'),
(70, 'Yiara Omara', 'commented on your book.', 1, 1, 1, 13, 'books', '2016-06-24T20:05:35.712Z'),
(71, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 12, 'books', '2016-06-24T20:34:35.586Z'),
(72, 'Tareq El-Masri', 'commented on your book.', 3, 1, 0, 12, 'books', '2016-06-24T20:40:59.350Z'),
(73, 'Tareq El-Masri', 'commented on your book.', 3, 1, 0, 7, 'books', '2016-06-24T20:43:03.009Z'),
(74, 'Tareq El-Masri', 'commented on your book.', 3, 1, 0, 7, 'books', '2016-06-24T21:18:47.057Z'),
(75, 'Tareq El-Masri', 'commented on your book.', 3, 1, 0, 7, 'books', '2016-06-24T21:20:44.059Z'),
(76, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 7, 'books', '2016-06-24T21:20:52.233Z'),
(77, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 7, 'books', '2016-06-24T21:22:14.570Z'),
(78, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 7, 'books', '2016-06-24T21:55:20.234Z'),
(79, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 7, 'books', '2016-06-24T21:56:16.607Z'),
(80, 'Tareq El-Masri', 'commented on your book.', 3, 1, 1, 7, 'books', '2016-06-24T21:56:53.882Z');

-- --------------------------------------------------------

--
-- Table structure for table `ratings`
--

CREATE TABLE `ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ratings`
--

INSERT INTO `ratings` (`id`, `user_id`, `target_user_id`, `rating`) VALUES
(1, 4, 1, 4),
(2, 1, 3, 3),
(3, 1, 0, 3),
(4, 1, 5, 3);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `report` text NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `user_id`, `target_user_id`, `report`, `seen`, `timestamp`) VALUES
(5, 4, 1, 'She''s mean', 1, '2016-06-10T19:02:27.906Z'),
(6, 4, 1, 'She''s mean', 1, '2016-06-10T19:02:27.906Z'),
(7, 4, 1, 'She''s mean', 1, '2016-06-10T19:02:27.906Z'),
(8, 4, 1, 'She said fuck you :(', 1, '2016-06-10T19:02:27.906Z');

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE `requests` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `author` text NOT NULL,
  `user_id` int(11) NOT NULL,
  `image_path` text NOT NULL,
  `rating` int(11) NOT NULL,
  `ratings_count` int(11) NOT NULL,
  `goodreads_id` int(100) NOT NULL,
  `isbn` varchar(50) NOT NULL,
  `timestamp` varchar(30) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `comments_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`id`, `name`, `author`, `user_id`, `image_path`, `rating`, `ratings_count`, `goodreads_id`, `isbn`, `timestamp`, `status`, `comments_count`) VALUES
(1, 'Bo Knows Bo', 'Bo Jackson', 4, 'http://d.gr-assets.com/books/1328327462m/228110.jpg', 4, 624, 228110, '', '0', 1, 0),
(2, 'Young Fu of the Upper Yangtze', 'Elizabeth Foreman Lewis', 4, 'http://d.gr-assets.com/books/1316728379m/1195367.jpg', 4, 2908, 1195367, '', '1464967139', 1, 0),
(6, 'Animal Farm / 1984', 'George Orwell', 4, 'http://d.gr-assets.com/books/1327959366m/5472.jpg', 4, 89234, 5472, '', '1464971566', 1, 0),
(7, 'Snowdrops', 'A.D. Miller', 4, 'http://d.gr-assets.com/books/1329600686m/9579671.jpg', 3, 6750, 9579671, '', '2016-06-04T21:26:47.453Z', 1, 0),
(8, 'Winnie-the-Pooh (Winnie-the-Pooh, #1)', 'A.A. Milne', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 197851, 99107, '', '2016-06-04T21:28:20.213Z', 1, 0),
(9, 'Ada, or Ardor: A Family Chronicle', 'Vladimir Nabokov', 4, 'http://d.gr-assets.com/books/1327867767m/12187.jpg', 4, 6751, 12187, '', '2016-06-04T21:29:07.757Z', 1, 0),
(10, 'Alex + Ada, Vol. 1', 'Jonathan Luna', 4, 'http://d.gr-assets.com/books/1400878535m/21823465.jpg', 4, 5161, 21823465, '', '2016-06-04T21:32:28.575Z', 1, 0),
(11, 'The A.B.C. Murders (Hercule Poirot, #13)', 'Agatha Christie', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 46859, 16322, '', '2016-06-04T21:40:42.110Z', 1, 0),
(12, 'Dr. Seuss''s ABC: An Amazing Alphabet Book! (Bright and Early Board Books)', 'Dr. Seuss', 4, 'http://d.gr-assets.com/books/1435186982m/17797344.jpg', 4, 32067, 17797344, '', '2016-06-04T21:41:37.706Z', 1, 0),
(13, 'The ABC''s of Kissing Boys', 'Tina Ferraro', 4, 'http://d.gr-assets.com/books/1320397036m/3596174.jpg', 4, 1581, 3596174, '', '2016-06-04T21:43:02.668Z', 1, 0),
(14, 'ABC of Reading', 'Ezra Pound', 4, 'http://s.gr-assets.com/assets/nophoto/book/111x148-bcc042a9c91a29c1d680899eff700a03.png', 4, 1158, 145108, '', '2016-06-04T21:43:29.307Z', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(50) NOT NULL,
  `email` varchar(30) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `img` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(40) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `city` varchar(80) NOT NULL,
  `college` varchar(100) NOT NULL,
  `bio` text NOT NULL,
  `fb_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `ip_address` int(10) NOT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  `online` tinyint(1) NOT NULL DEFAULT '0',
  `rating` int(11) NOT NULL DEFAULT '0',
  `ratings_count` int(11) NOT NULL DEFAULT '0',
  `gender` tinyint(4) NOT NULL DEFAULT '1',
  `reports_count` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(4) NOT NULL DEFAULT '0',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `fb_id` (`fb_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `email`, `username`, `password`, `img`, `salt`, `phone`, `city`, `college`, `bio`, `fb_id`, `active`, `activation_code`, `forgotten_password_code`, `remember_code`, `ip_address`, `created_on`, `last_login`, `online`, `rating`, `ratings_count`, `gender`, `reports_count`, `suspended`, `isAdmin`) VALUES
(1, 'Yiara Omara', 'yiaraomara@gmail.com', '', '13eb8ebedc3452e042b46fc9ed7c36c32605c30e', 'resources/users_images/1-1466798688.jpg', '', '01118085510', 'Egypt', 'BIS Helwan Uni.', 'A passionate designer who can effectively design your product information on the web and make user’s life easier.', 0, 1, NULL, NULL, NULL, 0, 0, 1466843072, 0, 4, 2, 1, 13, 0, 0),
(3, 'Tareq El-Masri', 'MASRIAPPS@GMAIL.COM', 'tareq', 'fbd26362246b235470aac37f277e5ac899528eac', '', '', '01097355899', 'Cairo', '', '', NULL, 1, NULL, NULL, NULL, 0, 1461612864, 1466800446, 0, 3, 1, 0, 0, 0, 0),
(4, 'Tareq El-Masri', 'tareq@masri.me', 'tareq Afifi', 'e4ffee75fe53c0b14ea606a51fb03d74205c037c', 'resources/users_images/4-1465063070.jpg', '', '01097355899', 'Cairo', 'AOU', '', NULL, 1, NULL, NULL, 'd001e23d306b7ffc7710827998a9554ca958e218', 0, 1461663638, 1466796867, 0, 0, 0, 0, 0, 0, 1),
(5, 'LOL', 'Helahop@hopa.xxx', 'ya maw', 'bb2856047807afe5609914834f8311d5', 'resources/users_images/5-1466798317.jpg', '', '', 'XXX City', '', 'My fucking awesome bio', NULL, 1, NULL, NULL, NULL, 0, 1466797065, 1466797105, 0, 3, 1, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(3, 1, 1),
(4, 3, 1),
(5, 4, 1),
(6, 5, 2);

-- --------------------------------------------------------

--
-- Table structure for table `wall`
--

CREATE TABLE `wall` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `target_user_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `activity` text NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` varchar(50) NOT NULL,
  `comments_count` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `wall`
--

INSERT INTO `wall` (`id`, `user_id`, `target_user_id`, `comment`, `activity`, `seen`, `timestamp`, `comments_count`) VALUES
(1, 4, 4, 'dadad', 'posted on his wall.', 0, '2016-06-06T20:02:23.810Z', 2),
(2, 4, 4, 'Holy fuck', 'his', 0, '2016-06-06T23:22:13.913Z', 0),
(3, 4, 4, 'Hi', 'posted on his wall.', 0, '2016-06-06T23:23:10.819Z', 0),
(4, 4, 1, 'Hello </3', 'posted on {state} wall.', 0, '2016-06-16T14:00:39.081Z', 0);
