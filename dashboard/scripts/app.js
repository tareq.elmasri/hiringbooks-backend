! function() {
    "use strict";
    angular.module("hiringbooksAngular", ["ngAnimate", "ngCookies", "ngTouch", "ngSanitize", "ngMessages", "ngAria", "restangular", "ui.router", "ui.bootstrap", "luegg.directives", "angular-ladda", "ngImgCrop", "naif.base64", "vkEmojiPicker", "ui.bootstrap.popover", "angular-loading-bar", "ui-notification", "angular-web-notification", "slickCarousel", "ngColorThief"])
}(),
function() {
    "use strict";

    function e(e, s, p, o, a, n, i, r, l, c, u) {
        var d = this,
            m = io(u);
        e.user = {}, e.users = [], e.selectedUser = {}, e.online_users = [], e.default_img = "assets/images/default-user.png", d.update_online_users = function() {
            _.each(e.users, function(s, t) {
                e.online_users.indexOf(s.id) > -1 ? e.users[t].online = "online" : e.users[t].online = "offline", n.get("?id=" + e.users[t].id).then(function(s) {
                    s.status !== !1 ? (e.users[t].books = s.books, e.users[t].books_comments = s.comments) : (e.users[t].books = [], e.users[t].books_comments = [])
                }), i.get("?id=" + e.users[t].id).then(function(s) {
                    s.status !== !1 ? (e.users[t].requests = s.requests, e.users[t].requests_comments = s.comments) : (e.users[t].requests = [], e.users[t].requests_comments = [])
                })
            }), e.$$phase || e.$broadcast("users_updated")
        }, m.on("message", function(s) {
            "onlineUsers" == s.title && _.isArray(s.data) ? a.getList().then(function(t) {
                e.users = t, e.online_users = s.data, d.getCurrentUser(), d.update_online_users()
            }) : "chatMessage" == s.title && _.isObject(s.data) || "socketid" == s.title && _.isString(s.data) && (e.socketID = s.data)
        }), d.getCurrentUser = function() {
            a.current_user().then(function(s) {
                m.emit("subscribe", s.id);
                e.user = s, e.user.password = "", e.user.online = "online", e.user.messages_unseen = [], e.user.notifications_unseen = 0, _.each(s.notifications, function(s) {
                    0 == s.seen && e.user.notifications_unseen++
                });
                var o = [];
                _.each(s.messages, function(s) {
                    0 == s.seen && _.each(e.users, function(t, a) {
                        t.id == s.user_id && (e.users[a].unseen_messages || (e.users[a].unseen_messages = 0), o.push(a))
                    })
                }), _.each(e.users, function(t) {
                    var a = _.findLastIndex(o, function(s) {
                        return t.id == e.users[s].id
                    });
                    a >= 0 && e.user.messages_unseen.push(s.messages[a])
                }), n.get("?id=" + e.user.id).then(function(s) {
                    s.status !== !1 ? (e.user.books = s.books, e.user.books_comments = s.comments) : (e.user.books = [], e.user.books_comments = []), e.$$phase || e.$broadcast("user_updated")
                }), i.get("?id=" + e.user.id).then(function(s) {
                    s.status !== !1 ? (e.user.requests = s.requests, e.user.requests_comments = s.requests_comments) : (e.user.requests = [], e.user.requests_comments = []), e.$$phase || e.$broadcast("user_updated")
                }), d.updateState(), m.emit("add user", e.user.id), "home" == p.current.name && p.go("user", {
                    user_id: e.user.id
                })
            }, function(e) {
                404 == e.status && window.location.assign("/auth")
            })
        }, m.on("user joined", function(s) {
            e.online_users = s.onlineUsers, e.users && e.users.length > 0 && d.update_online_users()
        }), d.updateState = function() {
            if (p.params.user_id && p.params.user_id != e.user.id) {
                var s = _.findIndex(e.users, function(e) {
                    return e.id == p.params.user_id
                });
                e.selectedUserIsMe = !1, e.selectedUser = e.users[s]
            } else e.selectedUserIsMe = !0, e.selectedUser = e.user;
            e.gender = 1 == e.selectedUser.gender ? "her" : "his";
            e.state = p.current.name
        }, e.$on("$routeChangeStart", d.updateState), d.global = e, d.addChatWindow = function(s, t) {
            var o = {
                user_id: s.id,
                user_index: t,
                current_user: e.user.id,
                current_index: _.findIndex(e.users, function(s) {
                    return s.id == e.user.id
                })
            };
            s.messages = [], s.message_offset = 0, r.get("?user_id=" + e.user.id + "&target_id=" + s.id + "&offset=" + s.message_offset).then(function(t) {
                _.isArray(t) && t.length > 0 && (_.each(t, function(t) {
                    t.mine = t.user_id == e.user.id, t.time = moment(t.timestamp).format("h:mm a"), s.messages.push(t)
                }), s.message_offset += t.length), e.chat_windows.indexOf(s) < 0 && e.chat_windows.push(s)
            }), r.set_seen({
                user_id: e.user.id,
                target_id: s.id
            }).then(function(e) {
                console.log(e)
            })
        }, m.on("room_id", function(s) {
            console.log(e.user.id, s.user_id), -1 == _.findIndex(e.chat_windows, e.users[s.current_index]) && e.user.id == s.user_id && (console.log(e.users[s.current_index]), e.chat_windows.push(e.users[s.current_index]), e.$apply())
        }), s.navigateToLatestNotification = function() {
            e.user.notifications[0].navigate()
        }, m.on("new notification", function(o) {
            console.log("a7a" + o);
            var a = JSON.parse(o.notification),
                n = e.users[_.findIndex(e.users, function(e) {
                    return e.id == a.user_id
                })];
            a.navigate = function() {
                t.go(a.topic_type, {
                    user_id: a.target_user_id,
                    book_id: a.topic_id
                })
            }, a.user_id !== e.user.id && 0 !== a.notification.length && (e.user.notifications_unseen++, e.user.notifications.unshift(a), l.success({
                message: a.name + " " + a.notification,
                templateUrl: "app/main/notification.html",
                scope: s
            }), e.$apply(), e.focus || c.showNotification("Example Notification", {
                body: a.name + " " + a.notification,
                icon: n.img,
                onClick: function() {
                    a.navigate()
                },
                autoClose: 4e3
            }, function(e, s) {
                e ? console.log("Unable to show notification: " + e.message) : (console.log("Notification Shown."), setTimeout(function() {
                    s()
                }, 5e3))
            }))
        }), d.closeChatWindow = function(s) {
            e.chat_windows.splice(s, 1)
        }
        e.state = p.current.name;
    }
    e.$inject = ["$rootScope", "$scope", "$state", "$location", "users", "books", "Requests", "Messages", "Notification", "webNotification", "socketpath"], angular.module("hiringbooksAngular").controller("MainController", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("wall");
        return s.addRestangularMethod("set_seen", "post", "set_seen"), s.addRestangularMethod("delete_post", "post", "delete_post"), s.addRestangularMethod("comment_post", "post", "comment_post"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Wall", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("users");
        return s.addRestangularMethod("current_user", "get", "current_user"), s.addRestangularMethod("suspend_user", "post", "suspend_user"), s.addRestangularMethod("delete_user", "post", "delete_user"), s.addRestangularMethod("rate_user", "post", "rate_user"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("users", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("search");
        return s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Search", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("requests");
        return s.addRestangularMethod("delete_request", "post", "delete_request"), s.addRestangularMethod("status_book", "post", "status_book"), s.addRestangularMethod("comment_book", "post", "comment_book"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Requests", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("reports");
        return s.addRestangularMethod("set_seen", "post", "set_seen"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Reports", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("notifications");
        return s.addRestangularMethod("set_seen", "post", "set_seen"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Notifications", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("messages");
        return s.addRestangularMethod("set_seen", "post", "set_seen"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("Messages", e)
}(),
function() {
    "use strict";

    function e(e, s, t) {
        var o = s.defer();
        return {
            get: function(t, a) {
                var n = "http://www.goodreads.com/search/index.xml?key=3O7gQmdLlhZPzVH9qgXlZQ&q=";
                n += encodeURIComponent(t), o.resolve(), o = s.defer(), e.get("http://query.yahooapis.com/v1/public/yql", {
                    timeout: o.promise,
                    params: {
                        timeout: o.promise,
                        q: 'select * from xml where url="' + n + '"',
                        format: "json"
                    }
                }).success(function(e, s, t, o) {
                    a(e.query.results.GoodreadsResponse.search.results.work)
                })
            },
            set: function(e) {
                var s = _.isArray(e);
                s || (e = [e]), _.each(e, function(s, o) {
                    if (t.user && t.user.books && t.user.books.length) {
                        e[o].submitLoading = !1, e[o].commentMode = !1;
                        var a = _.findIndex(t.user.books, function(e) {
                                return e.goodreads_id == s.best_book.id.content
                            }),
                            n = _.findIndex(t.user.requests, function(e) {
                                return e.goodreads_id == s.best_book.id.content
                            });
                        a > -1 || n > -1 ? e[o].mine = !0 : e[o].mine = !1
                    }
                })
            }
        }
    }
    e.$inject = ["$http", "$q", "$rootScope"], angular.module("hiringbooksAngular").factory("Goodreads", e)
}(),
function() {
    "use strict";

    function e(e) {
        var s = e.all("books");
        return s.addRestangularMethod("delete_book", "post", "delete_book"), s.addRestangularMethod("status_book", "post", "status_book"), s.addRestangularMethod("comment_book", "post", "comment_book"), s.addRestangularMethod("update_price", "post", "update_price"), s
    }
    e.$inject = ["Restangular"], angular.module("hiringbooksAngular").service("books", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t) {
            var o = this;
            o.global = e, o.profileSettings = !1, o.messagesVisible = !1, o.notificationsVisible = !1, o.finduser = function(s) {
                return _.findIndex(e.users, function(e) {
                    return e.id == s.user_id
                })
            }, o.setNotificationsSeen = function() {
                o.notificationsVisible && _.each(o.global.user.notifications, function(e, s) {
                    0 == e.seen && t.set_seen({
                        id: e.id
                    }, function(e) {
                        e.status !== !1 && (o.global.user.notifications_unseen = 0, o.global.user.notifications[s].seen = !0)
                    })
                })
            }, o.openMessage = function(e, s) {
                e.seen = 1, o.global.user.messages_unseen.splice(s, 1), o.addchatwindow(o.global.users[o.finduser(e)]), o.messagesVisible = !1
            }, o.goToNotification = function(t) {
                s.go(t.topic_type, {
                    user_id: e.user.id,
                    book_id: t.topic_id
                })
            }
        }
        e.$inject = ["$rootScope", "$state", "Notifications"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/navbar/navbar.html",
            scope: {
                creationDate: "=",
                addchatwindow: "=",
                guest: "="
            },
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("navbar", e)
}(),
function() {
    "use strict";

    function e() {
        function e(h, s, t, o, a, n) {
            var i = this;
            i.shit = h;
            i.newReport = "", i.reportVisible = !1, i.navigations = [{
                name: "user",
                url: "#/activity/",
                title: "Acitivty"
            }, {
                name: "books",
                url: "#/books/",
                title: "Books"
            }, {
                name: "requests",
                url: "#/requests/",
                title: "Requests"
            }], h.$on("user_updated", function(t) {
                console.log(t), console.log(h.selectedUser, h.user), s.$$phase || s.$apply()
            }), i.submitReport = function(s) {
                i.newReport.length > 0 && (s.reports_count++, n.post({
                    user_id: h.user.id,
                    target_user_id: s.id,
                    report: i.newReport,
                    reports_count: s.reports_count,
                    timestamp: moment()
                }).then(function(t) {
                    t.status ? (i.newReport = "", i.reportVisible = !1, a.success({
                        message: "Your report has been submited successfully."
                    })) : a.failure({
                        message: "Something wrong has happened."
                    })
                }))
            }, i.enterSubmit = function(z, s) {
                13 == z.keyCode && i.submitReport(s)
            }, i.rate = function(v, t) {
                h.selectedUser.rating = parseFloat(h.selectedUser.rating), v = t ? h.selectedUser.rating + v + 1 : v + 1, console.log(typeof h.selectedUser.rating), o.rate_user({
                    user_id: h.user.id,
                    target_user_id: h.selectedUser.id,
                    ratings_count: h.selectedUser.ratings_count,
                    rating: v
                }).then(function(c) {
                    h.selectedUser.rating = c.rating
                    h.selectedUser.ratings_count = c.ratings_count
                }), h.selectedUser.rating = 0 == h.selectedUser.rating ? h.selectedUser.rating : h.selectedUser.rating = (h.selectedUser.rating + v) / 2, console.log(h.selectedUser.rating), h.$$phase || h.$apply()
            }, i.activated_link = t.current.name, i.activate_link = function(z) {
                i.activated_link = z
            }
        }
        e.$inject = ["$rootScope", "$scope", "$state", "users", "Notification", "Reports"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/sidebar/sidebar.html",
            scope: {
                addchatwindow: "=",
                user: "="
            },
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("sidebar", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o) {
            var a = this;
            a.picture = {}, a.imgParser = function(e, s) {
                a.openModal("data:image/jpeg;base64," + s.base64)
            }, a.openModal = function(e) {
                var s = t.open({
                    animation: !0,
                    controller: "ImageModalController",
                    templateUrl: "app/components/mainsettings/imageModal.html",
                    resolve: {
                        picture: function() {
                            return e
                        }
                    }
                });
                s.result.then(function(e) {
                    a.global.user.img = e
                })
            }, a.global = s, a.submitLoading = !1, a.passwordVisible = "password", a.submit = function() {
                a.submitLoading = !0;
                var e = {
                    id: s.user.id,
                    fullname: s.user.fullname,
                    phone: s.user.phone,
                    city: s.user.city,
                    college: s.user.college,
                    img: s.user.img.replace("data:image/png;base64,", ""),
                    bio: s.user.bio
                };
                s.user.password.length > 0 && (e.password = s.user.password), o.post(e).then(function(e) {
                    s.user = e, s.user.password = "", a.submitLoading = !1, s.$broadcast("user_updated")
                })
            }
        }
        e.$inject = ["$scope", "$rootScope", "$uibModal", "users"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/mainsettings/mainsettings.html",
            scope: {
                user: "@",
                addchatwindow: "="
            },
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("mainSettings", e)
}(),
function() {
    "use strict";

    function e(e, s, t) {
        e.picture = t, e.croppedPicture = "", e.ok = function() {
            s.close(e.croppedPicture)
        }, e.cancel = function() {
            s.dismiss("cancel")
        }
    }
    e.$inject = ["$scope", "$uibModalInstance", "picture"], angular.module("hiringbooksAngular").controller("ImageModalController", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i) {
            var r = this;
            r.global = s, r.search = "", r.selectedTab = 0, r.searchResults = [], r.global.keyword = "", r.global.search = function(e) {
                r.searchResults = [], n.get("?keyword=" + e).then(function(e) {
                    e.status !== !1 && (r.searchResults = e)
                }), i.get(e, function(e) {
                    r.searchResults.goodreads = e, i.set(r.searchResults.goodreads)
                })
            }, r.global.getSearch = function(e) {
                13 == e.keyCode && (e.preventDefault(), t.go(s.guest ? "guestsearch" : "search", {
                    keyword: e.target.value
                }), e.target.value.length > 0 && r.global.search(e.target.value.length))
            }, r.searchForKeyword = function() {
                o.search().keyword && o.search().keyword.length > 0 && (r.global.keyword = o.search().keyword, r.global.search(o.search().keyword))
            }, r.navigateToUser = function(e) {
                t.go("user", {
                    user_id: e.id
                })
            }, r.searchForKeyword(), s.$on("$locationChangeSuccess", r.searchForKeyword)
        }
        e.$inject = ["$scope", "$rootScope", "$state", "$location", "$http", "Search", "Goodreads"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/mainsearch/mainsearch.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("mainSearch", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i) {
            var r = this;
            r.global = s, r.search = "", r.editMode = !1, r.isLoading = !1, r.commentMode = !1, r.searchedBooks = [], r.newComment = "", r.resultIsArray = !0, r.selectedRequest = {}, r.openRequest = function() {
                if (o.search().book_id > 0) {
                    var e = _.findIndex(s.selectedUser.requests, function(e) {
                        return e.id == o.search().book_id
                    });
                    e >= 0 && (r.commentMode = !0, r.global.selectedUser.requests[e].commentMode = !0, r.selectedRequest = r.global.selectedUser.requests[e])
                }
            }, s.$watchCollection("selectedUser", r.openRequest, !0), s.$on("$locationChangeSuccess", r.openRequest), r.searchBook = function(e) {
                e.target.value.length > 0 && (r.isLoading = !0, i.get(e.target.value, function(e) {
                    r.isLoading = !1, r.searchedBooks = e, i.set(r.searchedBooks)
                }))
            }, r.submit = function(e) {
                e.submitLoading = !0, e.user_id = s.user.id, e.fullname = s.user.fullname, e.timestamp = moment(), n.post(e).then(function(t) {
                    console.log(t), e.mine = !0, e.submitLoading = !1, s.user.requests.unshift(t)
                })
            }, r.comment = function(e, t) {
                13 == e.keyCode && (e.preventDefault(), e.target.value.length > 0 && (t.comments_count++, n.comment_request({
                    comment: e.target.value,
                    timestamp: moment(),
                    topic_id: t.id,
                    topic_type: "request",
                    user_id: s.user.id,
                    comments_count: t.comments_count,
                    fullname: s.user.fullname,
                    target_user_id: s.selectedUser.id
                }).then(function(t) {
                    s.selectedUser.requests_comments.unshift(t), e.target.value = ""
                })))
            }, r.findcommentuser = function(e) {
                return _.findIndex(s.users, function(s) {
                    return s.id == e.user_id
                })
            }, r.status = function(e) {
                var s = 0 == e.status ? 1 : 0;
                n.status_request({
                    id: e.id,
                    status: s
                }).then(function(t) {
                    e.status = s
                })
            }, r["delete"] = function(e) {
                n.delete_request({
                    id: s.user.requests[e].id
                }).then(function(t) {
                    s.user.requests.splice(e, 1)
                })
            }, r.blur = function() {
                r.commentMode = !1, r.selectedRequest.commentMode = !1, history.pushState && o.search("")
            }, r.focus = function(e) {
                r.commentMode = !0, r.selectedRequest = e, r.selectedRequest.commentMode = !0, history.pushState && o.search("book_id", e.id)
            }
        }
        e.$inject = ["$scope", "$rootScope", "$state", "$location", "$http", "Requests", "Goodreads"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/mainrequests/mainrequests.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("mainRequests", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i, r) {
            var l = this;
            l.global = s, l.search = "", l.editMode = !1, l.isLoading = !1, l.commentMode = !1, l.searchedBooks = [], l.newComment = "", l.resultIsArray = !0, l.selectedBook = {}, l.openBook = function() {
                if (a.search().book_id > 0) {
                    var e = _.findIndex(s.selectedUser.books, function(e) {
                        return e.id == a.search().book_id
                    });
                    e >= 0 && (l.commentMode = !0, l.global.selectedUser.books[e].commentMode = !0, l.selectedBook = l.global.selectedUser.books[e])
                }
            }, s.$watchCollection("selectedUser", l.openBook, !0), s.$on("$locationChangeSuccess", l.openBook), l.searchBook = function(e) {
                e.target.value.length > 0 && (l.isLoading = !0, r.get(e.target.value, function(e) {
                    l.isLoading = !1, l.searchedBooks = e, r.set(l.searchedBooks)
                }))
            }, l.submit = function(e) {
                e.submitLoading = !0, e.user_id = s.user.id, e.fullname = s.user.fullname, e.timestamp = moment();
                var o = t.open({
                    size: "sm",
                    animation: !0,
                    templateUrl: "app/components/mainbooks/bookModal.html",
                    controller: "BookModalController",
                    resolve: {
                        price: function() {
                            return 0
                        }
                    }
                });
                o.result.then(function(t) {
                    e.price = t, e.fullname = s.user.fullname, i.post(e).then(function(t) {
                        console.log(t), e.mine = !0, e.submitLoading = !1, s.user.books.unshift(t)
                    })
                })
            }, l.comment = function(e, t) {
                13 == e.keyCode && (e.preventDefault(), e.target.value.length > 0 && (t.comments_count++, i.comment_book({
                    comment: e.target.value,
                    timestamp: moment(),
                    topic_id: t.id,
                    topic_type: "book",
                    user_id: s.user.id,
                    comments_count: t.comments_count,
                    fullname: s.user.fullname,
                    target_user_id: s.selectedUser.id
                }).then(function(t) {
                    s.selectedUser.books_comments.unshift(t), e.target.value = ""
                })))
            }, l.findcommentuser = function(e) {
                return _.findIndex(s.users, function(s) {
                    return s.id == e.user_id
                })
            }, l.status = function(e) {
                var s = 0 == e.status ? 1 : 0;
                i.status_book({
                    id: e.id,
                    status: s
                }).then(function(t) {
                    e.status = s
                })
            }, l["delete"] = function(e) {
                i.delete_book({
                    id: s.user.books[e].id
                }).then(function(t) {
                    s.user.books.splice(e, 1)
                })
            }, l.blur = function() {
                l.commentMode = !1, l.selectedBook.commentMode = !1, history.pushState && a.search("")
            }, l.focus = function(e) {
                l.commentMode = !0, l.selectedBook = e, l.selectedBook.commentMode = !0, history.pushState && a.search("book_id", e.id)
            }, l.changePrice = function(e) {
                var s = t.open({
                    size: "sm",
                    animation: !0,
                    templateUrl: "app/components/mainbooks/bookModal.html",
                    controller: "BookModalController",
                    resolve: {
                        price: function() {
                            return e.price
                        }
                    }
                });
                s.result.then(function(s) {
                    i.update_price({
                        id: e.id,
                        price: s
                    }).then(function(t) {
                        e.price = s
                    })
                })
            }
        }
        e.$inject = ["$scope", "$rootScope", "$uibModal", "$state", "$location", "$http", "books", "Goodreads"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/mainbooks/mainbooks.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("mainBooks", e)
}(),
function() {
    "use strict";

    function e(e, s, t) {
        e.price = s, e.ok = function() {
            0 == e.price.length && (e.price = 0), t.close(e.price)
        }, e.okPress = function(s) {
            13 == s.keyCode && (0 == e.price.length && (e.price = 0), t.close(e.price))
        }, e.cancel = function() {
            t.dismiss("cancel")
        }
    }
    e.$inject = ["$scope", "price", "$uibModalInstance"], angular.module("hiringbooksAngular").controller("BookModalController", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s) {
            var t = this;
            t.global = s, s.$on("users_updated", function(o) {
                console.log(o), t.global.users = s.users, e.$$phase || e.$apply()
            }), t.user_search = ""
        }
        e.$inject = ["$scope", "$rootScope"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/leftsidebar/leftsidebar.html",
            scope: {
                user: "@",
                addchatwindow: "="
            },
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("leftSidebar", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i) {
            var r = this;
            r.global = s, r.search = "", r.selectedTab = 0, r.searchResults = [], r.global.keyword = "", r.global.getSearch = function($event) {
                if ($event.keyCode == 13) {
                    $event.preventDefault();
                    t.go(s.guest ? 'guestsearch' : 'search', {
                        keyword: $event.target.value
                    });
                    if ($event.target.value.length > 0) {
                        r.global.search($event.target.value.length);
                    }
                }
            }, r.global.search = function(e) {
                r.searchResults = [], n.get("?keyword=" + e).then(function(e) {
                    e.status !== !1 && (r.searchResults = e)
                }), i.get(e, function(e) {
                    r.searchResults.goodreads = e, i.set(r.searchResults.goodreads)
                })
            }, r.searchForKeyword = function() {
                o.search().keyword && o.search().keyword.length > 0 && (r.global.keyword = o.search().keyword, r.global.search(o.search().keyword))
            }, r.navigateToUser = function(e) {
                t.go("guestuser", {
                    user_id: e.id
                })
            }, r.searchForKeyword(), s.$on("$locationChangeSuccess", r.searchForKeyword)
        }
        e.$inject = ["$scope", "$rootScope", "$state", "$location", "$http", "Search", "Goodreads"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/guestsearch/guestsearch.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("guestSearch", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i, r) {
            var l = this;
            l.$apply = e.$apply, l.global = s, l.isLoading = !1, l.selectedBook = {}, l.booksLoaded = !1, l.slickConfig = {
                dots: !1,
                autoplay: !0,
                initialSlide: 3,
                infinite: !0,
                autoplaySpeed: 3e3,
                centerMode: !0,
                variableWidth: !0,
                method: {}
            }, l.redirect = function(url) {
                window.location.href = url;
            }, l.getPrimColor = function(e) {
                return "#F0EFE1" != e && "NaN,NaN,NaN" !== t("rgbToHsl")(e).join(",") ? (t("rgbToHsl")(e).join(",")) : void 0
            }, i.getList().then(function(e) {
                _.each(e, function(e) {
                    e.color = "#F0EFE1", e.getPrimColor = l.getPrimColor
                }), s.books = e, l.booksLoaded = !0, s.$$phase || s.$apply(), console.log(s.books)
            }), l.openBook = function() {
                if (a.search().book_id > 0) {
                    var e = _.findIndex(s.selectedUser.books, function(e) {
                        return e.id == a.search().book_id
                    });
                    e >= 0 && (l.commentMode = !0, l.global.selectedUser.books[e].commentMode = !0, l.selectedBook = l.global.selectedUser.books[e])
                }
            }, l.blur = function() {
                l.commentMode = !1, l.selectedBook.commentMode = !1, history.pushState && a.search("")
            }, l.focus = function(e) {
                l.commentMode = !0, l.selectedBook = e, l.selectedBook.commentMode = !0, history.pushState && a.search("book_id", e.id)
            }
        }
        e.$inject = ["$scope", "$rootScope", "$filter", "$state", "$location", "$http", "books", "Goodreads"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/guestbooks/guestbooks.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("guestBooks", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o) {
            var a = this,
                n = io(o);
            a.typing = !1, a.newMessage = "", a.global = e, a.user.minimized = !1, a.user.message_loading = !1, a.messages = a.user.messages ? a.user.messages : [], n.on("user_typing", function(t) {
                t.source == a.user.id && t.target == e.user.id && (a.user.typing = !0, s.$apply())
            }), n.on("user_stop_typing", function(t) {
                t.source == a.user.id && t.target == e.user.id && (a.user.typing = !1, s.$apply())
            }), n.on("private message", function(o) {
                o.user !== e.user.id && (o.time = moment(o.time).format("h:mm a"), a.messages.push(o), s.$apply(), t.set_seen({
                    user_id: e.user.id,
                    target_id: a.user.id
                }).then(function(e) {
                    console.log(e)
                }))
            }), a.preventNewLine = function(e) {
                13 == e.keyCode && e.preventDefault()
            }, a.sendMessage = function(s) {
                s.target.value.length > 0 ? n.emit("typing", {
                    source: e.user.id,
                    target: a.user.id
                }) : 0 == s.target.value.length && n.emit("stop typing", {
                    source: e.user.id,
                    target: a.user.id
                }), 13 == s.keyCode && (s.preventDefault(), s.target.value.length > 0 && (a.messages.push({
                    mine: !0,
                    message: s.target.value,
                    time: moment().format("h:mm a")
                }), t.post({
                    user_id: e.user.id,
                    target_id: a.user.id,
                    message: s.target.value,
                    seen: !1,
                    timestamp: moment()
                }).then(function(e) {
                    console.log(e)
                }), n.emit("new message", {
                    message: {
                        user: e.user.id,
                        target_user_id: a.user.id,
                        mine: !1,
                        message: s.target.value,
                        time: moment()
                    }
                }), s.target.value = ""))
            }, a.fetchMessages = function() {
                a.user.message_loading = !0, t.get("?user_id=" + e.user.id + "&target_id=" + a.user.id + "&offset=" + a.user.message_offset).then(function(s) {
                    a.user.message_loading = !1, s.length > 0 && (a.user.message_offset += s.length, _.each(s, function(s) {
                        s.mine = s.user_id == e.user.id, s.time = moment(s.timestamp).format("h:mm a"), a.user.messages.unshift(s)
                    }))
                })
            }
        }
        e.$inject = ["$rootScope", "$scope", "Messages", "socketpath"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/chat/chat.html",
            scope: {
                user: "=",
                index: "=",
                closechatwindow: "="
            },
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("chat", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n, i, r) {
            var l = this;
            l.global = s, l.selectedTab = 0, s.unseen_reports = 0, i.getList().then(function(e) {
                s.reports = e, _.each(e, function(e) {
                    "0" === e.seen && s.unseen_reports++
                }), l.setSeen(), s.$$phase || s.$apply()
            }), l.deleteUser = function(e, t) {
                n.delete_user({
                    id: e.id
                }).then(function(e) {
                    e.status !== !1 && (s.users.splice(t, 1), r.success({
                        message: "User deleted successfully."
                    }))
                })
            }, l.suspendUser = function(e) {
                n.suspend_user({
                    id: e.id,
                    suspended: 0 == e.suspended ? 1 : 0
                }).then(function(s) {
                    e.suspended = 0 == e.suspended ? 1 : 0, r.success({
                        message: "User " + e.suspended === 0 ? "unsuspended" : "suspended successfully."
                    })
                })
            }, l.setSeen = function() {
                i.set_seen().then(function(e) {
                    status !== !1 && (s.unseen_reports = 0)
                })
            }, l.findUser = function(e) {
                var t = _.find(s.users, function(s) {
                    return s.id == e
                });
                return t
            }, l.navigateToUser = function(e) {
                t.go("user", {
                    user_id: e.id
                })
            }
        }
        e.$inject = ["$scope", "$rootScope", "$state", "$location", "$http", "users", "Reports", "Notification"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/admin/admin.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("admin", e)
}(),
function() {
    "use strict";

    function e() {
        function e(e, s, t, o, a, n) {
            var i = this;
            i.global = s, i.selectedTab = 0, i.newWallPost = "", i.editMode = !1, i.commentMode = !1, i.submitLoading = !1, i.unseen_posts = 0, i.newComment = "", i.searchedPost = {}, i.setSeen = function() {
                _.each(s.user.wall, function(e) {
                    0 == e.seen && i.unseen_posts++
                }), n.set_seen({
                    id: s.user.id
                }, function(e) {
                    status !== !1 && (i.unseen_posts = 0)
                })
            }, i.openPost = function() {
                if (o.search().post_id > 0) {
                    var e = _.findIndex(s.selectedUser.wall, function(e) {
                        return e.id == o.search().post_id
                    });
                    e >= 0 && (i.commentMode = !0, i.global.selectedUser.posts[e].commentMode = !0, i.selectedBook = i.global.selectedUser.posts[e])
                }
            }, s.$watchCollection("selectedUser", i.openPost, !0), s.$on("$locationChangeSuccess", i.openPost), i.stateReplace = function(e) {
                return e.replace("{state}", s.selectedUserIsMe ? "your" : s.selectedUser.fullname.split(" ")[0] + "'s")
            }, i.findUser = function(e) {
                var t = _.find(s.users, function(s) {
                    return s.id == e
                });
                return t
            }, i.navigateToActivity = function(e) {
                t.go(e.topic_type, {
                    user_id: e.user_id,
                    post_id: e.topic_id
                })
            }, i.navigateToUser = function(e) {
                t.go("user", {
                    user_id: e.id
                })
            }, i.getActivity = function(e) {
                var t = _.find(s.selectedUser[e.topic_type], function(s) {
                    return e.topic_id == s.id
                });
                return t
            }, i.writeWallPost = function(e) {
                if (13 == e.keyCode && (e.preventDefault(), e.target.value.length > 0)) {
                    var t = s.selectedUserIsMe ? s.gender : "{state}";
                    n.post({
                        comment: e.target.value,
                        timestamp: moment(),
                        user_id: s.user.id,
                        seen: !1,
                        activity: "posted on " + t + " wall.",
                        target_user_id: s.selectedUser.id
                    }).then(function(t) {
                        s.selectedUser.wall.unshift(t), e.target.value = ""
                    })
                }
            }, i.comment = function(e, t) {
                13 == e.keyCode && (e.preventDefault(), e.target.value.length > 0 && (t.comments_count++, n.comment_post({
                    comment: e.target.value,
                    timestamp: moment(),
                    topic_id: t.id,
                    topic_type: "wall",
                    user_id: s.user.id,
                    comments_count: t.comments_count,
                    fullname: s.user.fullname,
                    target_user_id: s.selectedUser.id
                }).then(function(t) {
                    s.selectedUser.wall_comments.unshift(t), e.target.value = ""
                })))
            }, i["delete"] = function(e) {
                n.delete_post({
                    id: s.user.wall[e].id
                }).then(function(t) {
                    s.user.wall.splice(e, 1)
                })
            }, i.blur = function() {
                i.commentMode = !1, i.selectedPost.commentMode = !1, history.pushState && o.search("")
            }, i.focus = function(e) {
                i.commentMode = !0, i.selectedPost = e, i.selectedPost.commentMode = !0, history.pushState && o.search("post_id", e.id)
            }
        }
        e.$inject = ["$scope", "$rootScope", "$state", "$location", "$http", "Wall"];
        var s = {
            restrict: "E",
            templateUrl: "app/components/activity/activity.html",
            scope: {},
            controller: e,
            controllerAs: "vm",
            bindToController: !0
        };
        return s
    }
    angular.module("hiringbooksAngular").directive("activity", e)
}(),
function() {
    "use strict";

    function e(e, s, t, o, a, n, i, r, l, c, u) {
        var d = this;
        d.global = e, e.state = t.current.name;
        a.current_user().then(function(s) {
            if (s.status !== 404) {
                console.log(s);
                e.user = s, e.user.password = "", e.user.online = "online", e.user.messages_unseen = [], e.user.notifications_unseen = 0, _.each(s.notifications, function(s) {
                    0 == s.seen && e.user.notifications_unseen++
                });
                var o = [];
                _.each(s.messages, function(s) {
                    0 == s.seen && _.each(e.users, function(t, a) {
                        t.id == s.user_id && (e.users[a].unseen_messages || (e.users[a].unseen_messages = 0), o.push(a))
                    })
                }), _.each(e.users, function(t) {
                    var a = _.findLastIndex(o, function(s) {
                        return t.id == e.users[s].id
                    });
                    a >= 0 && e.user.messages_unseen.push(s.messages[a])
                }), n.get("?id=" + e.user.id).then(function(s) {
                    s.status !== !1 ? (e.user.books = s.books, e.user.books_comments = s.comments) : (e.user.books = [], e.user.books_comments = []), e.$$phase || e.$broadcast("user_updated")
                }), i.get("?id=" + e.user.id).then(function(s) {
                    s.status !== !1 ? (e.user.requests = s.requests, e.user.requests_comments = s.requests_comments) : (e.user.requests = [], e.user.requests_comments = []), e.$$phase || e.$broadcast("user_updated")
                })
            }
            e.guest = false;
            if (!e.$$phase) {
                e.$apply();
            }
        }, function(n) {
            console.log(n);
            e.guest = true;
            if (!e.$$phase) {
                e.$apply();
            }
        });
        console.log(e, d.global);
    }
    e.$inject = ["$rootScope", "$scope", "$state", "$location", "users", "books", "Requests", "Messages", "Notification", "webNotification", "socketpath"], angular.module("hiringbooksAngular").controller("GuestController", e)
}(),
function() {
    "use strict";

    function e(e, s) {
        e.chat_windows = [], 0 !== window.Notification.permission && window.Notification.requestPermission(), s.onfocus = function() {
            e.focus = !0
        }, s.onblur = function() {
            e.focus = !1
        }
    }
    e.$inject = ["$rootScope", "$window"], angular.module("hiringbooksAngular").run(e)
}(),
function() {
    "use strict";

    function e(e, s) {
        e.state("home", {
            url: "/",
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("user", {
            url: "/users/:user_id?post_id",
            params: {
                user_id: ""
            },
            reloadOnSearch: !1,
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("admin", {
            url: "/admin/:user_id",
            params: {
                user_id: ""
            },
            reloadOnSearch: !1,
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("books", {
            url: "/books/:user_id?book_id",
            params: {
                user_id: ""
            },
            reloadOnSearch: !1,
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("requests", {
            url: "/requests/:user_id?book_id",
            params: {
                user_id: ""
            },
            reloadOnSearch: !1,
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("settings", {
            url: "/settings",
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("search", {
            url: "/search?keyword",
            reloadOnSearch: !1,
            templateUrl: "app/main/main.html",
            controller: "MainController",
            controllerAs: "vm"
        }).state("guestsearch", {
            url: "/guestsearch?keyword",
            reloadOnSearch: !1,
            templateUrl: "app/guest/guest.html",
            controller: "GuestController",
            controllerAs: "vm"
        }).state("guestbooks", {
            url: "/guestbooks?bookid",
            reloadOnSearch: !1,
            templateUrl: "app/guest/guest.html",
            controller: "GuestController",
            controllerAs: "vm"
        }), s.otherwise("/")
    }
    e.$inject = ["$stateProvider", "$urlRouterProvider"], angular.module("hiringbooksAngular").config(e)
}(),
function() {
    "use strict";
    angular.module("hiringbooksAngular").filter("round", function() {
        return function(e) {
            return Math.round(parseFloat(e))
        }
    }).filter("relativeDate", function() {
        return function(e) {
            return moment(e).fromNow()
        }
    }).filter("rgbToHsl", function() {
        return function(e) {
            var s = e[0];
            s /= 255;
            var t = e[1];
            t /= 255;
            var o = e[2];
            o /= 255;
            var a, n, i = Math.max(s, t, o),
                r = Math.min(s, t, o),
                l = (i + r) / 2;
            if (i == r) a = n = 0;
            else {
                var c = i - r;
                switch (n = l > .5 ? c / (2 - i - r) : c / (i + r), i) {
                    case s:
                        a = (t - o) / c + (o > t ? 6 : 0);
                        break;
                    case t:
                        a = (o - s) / c + 2;
                        break;
                    case o:
                        a = (s - t) / c + 4
                }
                a /= 6
            }
            return _.isArray(e) ? [Math.floor(360 * a), Math.floor(100 * n) + "%", Math.floor(100 * l) - 40 + "%"] : void 0
        }
    }).filter("range", function() {
        return function(e) {
            for (var s = [], t = 0; t < Math.round(parseFloat(e)); t++) s.push(t);
            return s
        }
    })
}(),
function() {
    "use strict";
    angular.module("hiringbooksAngular").directive("autoFocus", function() {
        return {
            link: {
                pre: function(e, s, t) {},
                post: function(e, s, t) {
                    s[0].focus()
                }
            }
        }
    }).directive("checkForPagination", function() {
        return {
            link: function(e, s, t) {
                s.scroll(function() {
                    if (s.scrollTop() <= 0) {
                        var o = e.$eval(t.checkForPagination);
                        o(), s.css({
                            scrollTop: 5
                        })
                    }
                })
            }
        }
    }).directive("fileButton", function() {
        return {
            link: {
                pre: function(e, s, t) {},
                post: function(e, s, t) {
                    s.bind("click", function() {
                        document.getElementById("fileInput").click()
                    })
                }
            }
        }
    })
}(),
function() {
    "use strict";
    angular.module("hiringbooksAngular").constant("backendpath", "localhost").constant("socketpath", "localhost:7000").constant("malarkey", malarkey).constant("moment", moment)
}(),
function() {
    "use strict";

    function e(e, s, t, o) {
        t.setBaseUrl("../api/v1/"), t.setDefaultHeaders({
            "Access-Control-Allow-Origin": "*"
        }), s.html5Mode(false), e.debugEnabled(!0), o.setOptions({
            delay: 1e4,
            startTop: 20,
            startRight: 10,
            verticalSpacing: 20,
            horizontalSpacing: 20,
            positionX: "left",
            positionY: "bottom"
        })
    }
    e.$inject = ["$logProvider", "$locationProvider", "RestangularProvider", "NotificationProvider"], angular.module("hiringbooksAngular").config(e)
}(), angular.module("hiringbooksAngular").run(["$templateCache", function(e) {
    e.put("app/guest/guest.html", '<navbar guest=vm.global.guest></navbar><div class=guest><guest-search ng-show="vm.global.state == \'guestsearch\'"></guest-search><guest-books ng-show="vm.global.state == \'guestbooks\'"></guest-books><guest-requests ng-show="vm.global.state == \'guestrequests\'"></guest-requests><guest-users ng-show="vm.global.state == \'guestusers\'"></guest-users><div class=preloader ng-hide=vm.global.state><svg class=spinner width=64px height=64px viewBox="0 0 66 66"><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg></div></div><!-- <footer id="footer" class="clearfix">\n  <div id="subfooter">\n      <div class="container">\n          <div id="copyrights" class="col-xs-12 col-sm-4 col-md-3">\n              <p>2016 © All Rights Reserved</p>\n          </div>\n      </div>\n  </div>\n</footer> -->'),
        e.put("app/main/main.html", '<navbar addchatwindow=vm.addChatWindow></navbar><sidebar user="vm.global.selectedUser" addchatwindow=vm.addChatWindow></sidebar><div class=main><admin ng-show="vm.global.state == \'admin\'"></admin><activity ng-show="vm.global.state == \'user\'"></activity><main-search ng-show="vm.global.state == \'search\'"></main-search><main-books ng-show="vm.global.state == \'books\'"></main-books><main-requests ng-show="vm.global.state == \'requests\'"></main-requests><main-settings ng-show="vm.global.state == \'settings\'"></main-settings><div class=preloader ng-hide=vm.global.state><svg class=spinner width=64px height=64px viewBox="0 0 66 66"><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg></div></div><left-sidebar user=vm.user chats=vm.chat_windows addchatwindow=vm.addChatWindow></left-sidebar><chat ng-repeat="user in vm.global.chat_windows" index=$index closechatwindow=vm.closeChatWindow user=user></chat><!-- <footer id="footer" class="clearfix">\n  <div id="subfooter">\n      <div class="container">\n          <div id="copyrights" class="col-xs-12 col-sm-4 col-md-3">\n              <p>2016 © All Rights Reserved</p>\n          </div>\n      </div>\n  </div>\n</footer> -->'),
        e.put("app/components/sidebar/sidebar.html", '<aside><span ng-style="{\'background-image\':\'url(\'+\'../\'+vm.user.img+\'), url(assets/images/default-user.png)\'}" class=profile-picture></span><div class=details><h2>{{vm.user.fullname}} <i class="fa fa-circle-o {{vm.user.online}}"></i></h2><h4>{{vm.user.college}}</h4><h4 ng-if=!vm.shit.selectedUserIsMe><i class="fa fa-star" ng-repeat="i in 5 | range" ng-class="{\'o\': vm.user.rating <= i}" ng-click=vm.rate(i)></i> {{vm.user.ratings_count}} voters</h4><h4 ng-if=vm.shit.selectedUserIsMe><i class="fa fa-star" ng-repeat="i in 5 | range" ng-class="{\'o\': vm.user.rating <= i}"></i> {{vm.user.ratings_count}} voters</h4><button ng-show="vm.user.id != vm.global.selectedUser.id" ng-click="vm.reportVisible = !vm.reportVisible" ng-keyup=$event.preventDefault()><i class="fa fa-legal"></i> Report<div class=report-bubble ng-class="{\'visible\': vm.reportVisible}"><i class=arrow></i><textarea ng-model=vm.newReport placeholder="Enter a report message..." ng-click=$event.stopPropagation() ng-keydown="$event.stopPropagation(); vm.enterSubmit($event, vm.user)" auto-focus></textarea><a href=javascript:void(0) ng-click="$event.stopPropagation(); vm.submitReport(vm.user);">Submit</a><div></div></div></button></div><div class=navigation><a ng-repeat="navigation in vm.navigations" ng-href="{{navigation.url + vm.user.id}}" ng-class="{\'active\':vm.activated_link == navigation.name}" ng-click=vm.activate_link(navigation.name)>{{vm.user.id != vm.user.user.id ? vm.user.fullname.split(" ")[0] + "\'s " : "My "}} {{navigation.title}}</a></div></aside>'),
        e.put("app/main/notification.html", '<div class="ui-notification custom-template" ng-click=navigateToLatestNotification()><a class="btn btn-small close-notification" ng-click=close()>×</a><div class=message ng-bind-html=message></div></div>'),
        e.put("app/components/activity/activity.html", '<div class=tabs><a href=javascript:void(0) class="tab half" ng-click="vm.selectedTab = 0" ng-class="{\'active\': vm.selectedTab == 0}">Wall <sub ng-if="vm.global.selectedUserIsMe && vm.unseen_posts != 0">{{vm.unseen_posts}}</sub></a> <a href=javascript:void(0) class="tab half" ng-click="vm.selectedTab = 1" ng-class="{\'active\': vm.selectedTab == 1}">Activity</a></div><div class=container ng-show="vm.selectedTab == 0"><div class="input-row full"><input type=text ng-model=vm.newWallPost placeholder="Write something to {{vm.global.selectedUserIsMe ? \'yourself\' : vm.global.selectedUser.fullname}}." ng-keyup=vm.writeWallPost($event)> <a emoji-picker=vm.newWallPost placement=bottom class=emojipicker></a></div><div class=splitter></div><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=empty ng-show="vm.global.selectedUser.wall.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=posts-row><div class="col post" ng-repeat="post in vm.global.selectedUser.wall" ng-class="{\'active\':post.commentMode}"><a href=javascript:void(0) ng-click=vm.navigateToUser(post.user_id)><img ng-src={{"../"+vm.findUser(post.user_id).img}} onerror="this.src=\'assets/images/default-user.png\'"></a><h3><a href=javascript:void(0) ng-click=vm.navigateToUser(post.user_id)>{{vm.findUser(post.user_id).fullname}}</a> {{vm.stateReplace(post.activity)}}</h3><small>{{post.timestamp | relativeDate}}</small><p>{{post.comment | emojify}}</p><h2 ng-click=vm.focus(post)><i class="fa fa-comments"></i> <span>{{post.comments_count}} comments</span></h2><button class=close-button ng-class="{\'visible\': post.commentMode}" ng-click=vm.blur()>×</button> <button class="large-button red" ladda=post.submitLoading ng-click=vm.delete($index)>DELETE</button><div class=comments><div class=comment-container><input type=text placeholder="WRITE A COMMENT" ng-keyup="vm.comment($event, post)" ng-model=vm.newComment> <a emoji-picker=vm.newComment placement=bottom class=emojipicker></a></div><div class=comment ng-repeat="comment in vm.global.selectedUser.wall_comments" ng-if="comment.topic_id == post.id"><img ng-src={{"../"+vm.findUser(comment.user_id).img}} alt="" onerror="this.src=\'assets/images/default-user.png\'"> <a ng-href=#/users/{{vm.findUser(comment.user_id).id}}>{{vm.findUser(comment.user_id).fullname}} <span class=split>&bull;</span> <span>{{comment.timestamp | relativeDate}}</span></a><p ng-bind-html="comment.comment | emojify"></p></div></div></div></div></div></div><div class=container ng-show="vm.selectedTab == 1"><div class=results-row><div class=empty ng-show="vm.global.selectedUser.activity.length == 0"><p>NO ACTIVITY WERE FOUND.</p></div><div class=activity-row><div class="col user" ng-repeat="activity in vm.global.selectedUser.activity" ng-click=vm.navigateToActivity(activity)><img class=profile-picture ng-src={{vm.getActivity(activity).image_path}} onerror="this.src=\'assets/images/default-user.png\'"><h3>{{vm.findUser(activity.user_id).fullname + " " + activity.activity}}</h3><h2>{{vm.getActivity(activity).name}}</h2></div></div></div></div>'),
        e.put("app/components/admin/admin.html", '<div class=tabs><a href=javascript:void(0) class="tab half" ng-click="vm.selectedTab = 0" ng-class="{\'active\': vm.selectedTab == 0}">Reports <sub ng-show="vm.global.unseen_reports != 0">{{vm.global.unseen_reports}}</sub></a> <a href=javascript:void(0) class="tab half" ng-click="vm.selectedTab = 1" ng-class="{\'active\': vm.selectedTab == 1}">Users</a></div><div class=container ng-show="vm.selectedTab == 0"><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=empty ng-show="vm.global.selectedUser.wall.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=reports-row><div class="col report" ng-repeat="report in vm.global.reports"><a href=javascript:void(0) ng-click=vm.navigateToUser(report.user_id)><img class=profile-picture-a ng-src={{"../"+vm.findUser(report.user_id).img}} onerror="this.src=\'assets/images/default-user.png\'"></a><h3><a href=javascript:void(0) ng-click=vm.navigateToUser(report.user_id)>{{vm.findUser(report.user_id).fullname}}</a> has reported <a href=javascript:void(0) ng-click=vm.navigateToUser(report.target_user_id)>{{vm.findUser(report.target_user_id).fullname}}.</a></h3><a href=javascript:void(0) ng-click=vm.navigateToUser(report.target_user_id)><img class=profile-picture-b ng-src={{"../"+vm.findUser(report.target_user_id).img}} onerror="this.src=\'assets/images/default-user.png\'"></a><small>{{report.timestamp | relativeDate}}</small><p>{{report.report}}</p></div></div></div></div><div class=container ng-show="vm.selectedTab == 1"><div class=results-row><div class=empty ng-show="vm.global.users.length == 0"><p>NO USERS WERE FOUND.</p></div><table class="table table-striped users-row"><thead><th>#</th><th>Name</th><th>Username</th><th>Email</th><th>Admin?</th><th>Reports</th><th>Suspention</th><th>Delete</th></thead><tr class="col user" ng-repeat="user in vm.global.users track by $index" ng-click=vm.navigateToUser(user) ng-hide="user.id == vm.global.user.id"><th>{{user.id}}</th><th>{{user.fullname}}</th><th>{{user.username}}</th><th>{{user.email}}</th><th>{{user.isAdmin ? "Yes" : "No"}}</th><th>{{user.reports_count}}</th><th><button class="btn btn-default" ng-click="$event.stopPropagation(); vm.suspendUser(user)">{{user.suspended == 0 ? \'Suspend\' : \'Unsuspend\'}}</button></th><th><button class="btn btn-warning" ng-click="$event.stopPropagation(); vm.deleteUser(user, $index)">Delete</button></th></tr></table></div></div>'),
        e.put("app/components/chat/chat.html", '<div class=chat-window ng-class="{\'minimized\': vm.user.minimized}" ng-style="{right: 240 + vm.index * 240 + (20 * vm.index)}"><header><i class="user-status fa fa-circle-o {{vm.user.online}}"></i> <span class=profile-picture ng-attr-style="background-image:url(\'../{{vm.user.img}}\'), url({{vm.global.default_img}})"></span> <a class=user-name ng-href=#/users/{{vm.user.id}}>{{vm.user.fullname}}</a> <a class=button ng-click=vm.closechatwindow(vm.index)><i class="fa fa-close"></i></a> <a class=button ng-click="vm.user.minimized = !vm.user.minimized"><i class="fa fa-minus"></i></a></header><div class=body ng-hide=vm.user.minimized scroll-glue check-for-pagination=vm.fetchMessages><svg class=spinner width=32px height=32px viewBox="0 0 66 66" ng-show=vm.user.message_loading><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg><div class=message ng-repeat="message in vm.messages" ng-class="{\'mine\':message.mine}"><span>{{message.message | emojify}}</span> <small>{{message.time}}</small></div><i ng-show=vm.user.typing>{{vm.user.fullname}} is typing...</i></div><footer ng-hide=vm.user.minimized><textarea placeholder="Enter a text here..." ng-model=vm.newMessage ng-keydown=vm.preventNewLine($event) ng-keyup=vm.sendMessage($event) auto-focus></textarea><a emoji-picker=vm.newMessage placement=top class=emojipicker></a></footer></div>'),
        e.put("app/components/guestbooks/guestbooks.html", '<div class=container><div class=preloader ng-hide=vm.booksLoaded><svg class=spinner width=64px height=64px viewBox="0 0 66 66"><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg></div><slick class=slider ng-if=vm.booksLoaded settings=vm.slickConfig><div class=slick-slide ng-repeat="book in vm.global.books" ng-style="{\'background\':\'rgb(\'+book.color.join(\',\')+\')\'}"><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src="../image?img={{book.image_path}}" color-thief color-thief-dominant=book.color></a><h2>{{book.name}}</h2><h3>by {{book.author}}</h3><h2 class=ratings><i class="fa fa-star" ng-repeat="i in book.rating | range" ng-attr-style="color: hsl({{book.getPrimColor(book.color)}})"></i> <i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><!-- <h2 ng-click="vm.focus(book)">\n        <i class="fa fa-comments"></i>\n        <span>{{book.comments_count}} comments</span>\n      </h2> --> <strong>£{{book.price}}</strong> <button ng-attr-style="color: hsl({{book.getPrimColor(book.color)}}); box-shadow: 0 5px 15px hsla({{book.getPrimColor(book.color)}}, 0.4);" ng-click="vm.redirect(\'#/books/\'+book.user_id+\'?book_id=\'+book.id)">See This Book</button></div></slick><div class=books-row ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class="col book" ng-repeat="book in vm.global.books" ng-class="{\'active\':book.commentMode}"><div class=body><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range" ng-style="{\'color\': \'hsl(\'+getPrimColor(book.color)+\')\'}"></i> <i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><h2 ng-click="vm.redirect(\'#/books/\'+book.user_id+\'?book_id=\'+book.id)"><i class="fa fa-comments"></i> <span>{{book.comments_count}} comments</span></h2><strong>£{{book.price}}</strong> <i class="fa fa-check-circle" ng-show=book.mine></i> <button class=close-button ng-class="{\'visible\': book.commentMode}" ng-click=vm.blur()>×</button> <button class="large-button half" ng-class="{\'orange\':book.status == 0}">{{book.status == 1 ? \'AVALIABLE\' : \'NOT AVALIABLE\'}}</button></div></div></div></div>'),
        e.put("app/components/guestsearch/guestsearch.html", '<div class=tabs><a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 0" ng-class="{\'active\': vm.selectedTab == 0}">Users <sub>{{vm.searchResults.users.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 1" ng-class="{\'active\': vm.selectedTab == 1}">Books <sub>{{vm.searchResults.books.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 2" ng-class="{\'active\': vm.selectedTab == 2}">Requests <sub>{{vm.searchResults.requests.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 3" ng-class="{\'active\': vm.selectedTab == 3}">Goodreads <sub>{{vm.searchResults.goodreads.length}}</sub></a></div><div class=container ng-show="vm.selectedTab == 1"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.books.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.books"><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><strong>£{{book.price}}</strong></div></div></div></div><div class=container ng-show="vm.selectedTab == 0"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.users.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.users.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col user" ng-repeat="user in vm.searchResults.users"><img class=profile-picture ng-src={{"../"+user.img}} onerror="this.src=\'assets/images/default-user.png\'"><h3>{{user.fullname}}</h3><h2>{{user.city}}</h2><h2><i class="fa fa-star" ng-repeat="i in user.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - user.rating | range)"></i> <span>{{user.ratings_count}} voters</span></h2></div></div></div></div><div class=container ng-show="vm.selectedTab == 2"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.requests.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.requests"><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2></div></div></div></div><div class=container ng-show="vm.selectedTab == 3"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.goodreads.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.goodreads"><a ng-href=//www.goodreads.com/book/show/{{book.best_book.id.content}} target=_blank><img ng-src={{book.best_book.image_url}}></a><h2>{{book.best_book.author.name}}</h2><h3>{{book.best_book.title}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.average_rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.average_rating | range)"></i> <span>{{book.ratings_count.content}} voters</span></h2></div></div></div></div>'),
        e.put("app/components/leftsidebar/leftsidebar.html", '<aside><div class=side-row><form class=search-container><i class="fa fa-search"></i> <input class=search placeholder=Search ng-model=vm.user_search></form></div><a href="" class="user side-row" ng-repeat="user in vm.global.users | filter: {fullname: vm.user_search}" ng-click="vm.addchatwindow(user, $index)"><img ng-src={{\'../\' + user.img}}" onerror="this.src=\'assets/images/default-user.png\'"> <span>{{user.fullname}}</span> <i class="fa fa-circle-o" ng-class=user.online></i></a></aside>'),
        e.put("app/components/mainbooks/bookModal.html", '<div class=modal-header><h4 class=modal-title>BOOK PRICE</h4></div><div class=modal-body><input type=text placeholder="Enter a book price" ng-model=price ng-keyup=okPress($event)><p>Leave the price as 0 if you offer it for rent.</p></div><div class=modal-footer><button ng-click=cancel() class="btn btn-warning">Cancel</button> <button ng-click=ok() class="btn btn-primary">Save</button></div>'),
        e.put("app/components/mainbooks/mainbooks.html", '<div class=container><h1>{{vm.editMode ? \'ADD A BOOK\' : \'BOOKS\'}}</h1><svg class=spinner width=32px height=32px viewBox="0 0 66 66" ng-class="{\'visible\':vm.isLoading}"><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg> <button class=add-button ng-class="{\'active\':vm.editMode}" ng-click="vm.editMode = !vm.editMode" ng-show=vm.global.selectedUserIsMe><i class="fa fa-plus"></i></button><div class=books-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.global.selectedUser.books.length == 0"><p>{{vm.global.selectedUserIsMe ? \'YOU\' : \'THE USER\'}} CURRENTLY DON\'T HAVE ANY BOOKS. <a href=javascript:void(0) ng-click="vm.editMode = true" ng-show=vm.global.selectedUserIsMe>ADD SOME BOOKS.</a></p></div><div class="col book" ng-repeat="book in vm.global.selectedUser.books" ng-class="{\'active\':book.commentMode}" ng-if=vm.resultIsArray><div class=body><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><h2 ng-click=vm.focus(book)><i class="fa fa-comments"></i> <span>{{book.comments_count}} comments</span></h2><strong ng-click=vm.changePrice(book) ng-if=vm.global.selectedUserIsMe ng-show=!book.commentMode>£{{book.price}}</strong> <strong ng-if="vm.global.selectedUser.id != vm.global.user.id">£{{book.price}}</strong> <i class="fa fa-check-circle" ng-show=book.mine></i> <button class=close-button ng-class="{\'visible\': book.commentMode}" ng-click=vm.blur()>×</button> <button class="large-button half" ng-class="{\'orange\':book.status == 0}" ladda=book.submitLoading ng-click=vm.status(book)>{{book.status == 1 ? \'AVALIABLE\' : \'NOT AVALIABLE\'}}</button> <button class="large-button half red" ladda=book.submitLoading ng-click=vm.delete($index)>DELETE</button></div><div class=comments><div class=comment-container><input type=text placeholder="WRITE A COMMENT" ng-keyup="vm.comment($event, book)" ng-model=vm.newComment> <a emoji-picker=vm.newComment placement=bottom class=emojipicker></a></div><div class=comment ng-repeat="comment in vm.global.selectedUser.books_comments" ng-if="comment.topic_id == book.id"><img ng-src={{"../"+vm.global.users[vm.findcommentuser(comment)].img}} onerror="this.src=\'assets/images/default-user.png" alt=""> <a ng-href=#/users/{{vm.global.users[vm.findcommentuser(comment)].id}}>{{vm.global.users[vm.findcommentuser(comment)].fullname}} <span class=split>&bull;</span> <span>{{comment.timestamp | relativeDate}}</span></a><p ng-bind-html="comment.comment | emojify"></p></div></div></div></div><div class=search-row ng-show=vm.editMode><div class="col full"><input type=text ng-model=vm.search placeholder="ENTER A BOOK, AUTHOR OR ISBN" ng-keyup=vm.searchBook($event)></div></div><div class=results ng-show="vm.editMode && vm.search.length > 0"><div class=splitter></div><h1>SEARCH RESULTS</h1><small>{{vm.searchedBooks.length}} results.</small><div class=search-row><div class="col book" ng-repeat="book in vm.searchedBooks" ng-if=vm.resultIsArray><a ng-href=//www.goodreads.com/book/show/{{book.best_book.id.content}} target=_blank><img ng-src={{book.best_book.image_url}}></a><h2>{{book.best_book.author.name}}</h2><h3>{{book.best_book.title}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.average_rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.average_rating | range)"></i> <span>{{book.ratings_count.content}} voters</span></h2><i class="fa fa-check-circle" ng-show=book.mine></i> <button class=large-button ladda=book.submitLoading ng-click=vm.submit(book) ng-disabled=book.mine>{{book.mine ? \'ADDED\' : \'ADD\'}} TO MY BOOKS</button></div></div></div></div>'),
        e.put("app/components/mainrequests/mainrequests.html", '<div class="container books-container"><h1>{{vm.editMode ? \'REQUEST A BOOK\' : \'REQUESTS\'}}</h1><svg class=spinner width=32px height=32px viewBox="0 0 66 66" ng-class="{\'visible\':vm.isLoading}"><circle class=path fill=none stroke-width=6 stroke-linecap=round cx=33 cy=33 r=30></circle></svg> <button class=add-button ng-class="{\'active\':vm.editMode}" ng-click="vm.editMode = !vm.editMode" ng-show=vm.global.selectedUserIsMe><i class="fa fa-plus"></i></button><div class=books-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.global.selectedUser.requests.length == 0"><p>{{vm.global.selectedUserIsMe ? \'YOU\' : \'THE USER\'}} CURRENTLY DON\'T HAVE ANY REQUESTS. <a href=javascript:void(0) ng-click="vm.editMode = true" ng-show=vm.global.selectedUserIsMe>REQUESTS SOME BOOKS.</a></p></div><div class="col book" ng-repeat="book in vm.global.selectedUser.requests" ng-class="{\'active\':book.commentMode}" ng-if=vm.resultIsArray><div class=body><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><h2 ng-click=vm.focus(book)><i class="fa fa-comments"></i> <span>{{book.comments_count}} comments</span></h2><i class="fa fa-check-circle" ng-show=book.mine></i> <button class=close-button ng-class="{\'visible\': book.commentMode}" ng-click=vm.blur()>×</button> <button class="large-button red" ladda=book.submitLoading ng-click=vm.delete($index)>DELETE</button></div><div class=comments><div class=comment-container><input type=text placeholder="WRITE A COMMENT" ng-keyup="vm.comment($event, book)" ng-model=vm.newComment> <a emoji-picker=vm.newComment placement=bottom class=emojipicker></a></div><div class=comment ng-repeat="comment in vm.global.selectedUser.requests_comments" ng-if="comment.topic_id == book.id"><img ng-src={{"../"+vm.global.users[vm.findcommentuser(comment)].img}} alt=""> <a ng-href=#/users/{{vm.global.users[vm.findcommentuser(comment)].id}}>{{vm.global.users[vm.findcommentuser(comment)].fullname}} <span class=split>&bull;</span> <span>{{comment.timestamp | relativeDate}}</span></a><p ng-bind-html="comment.comment | emojify"></p></div></div></div></div><div class=search-row ng-show=vm.editMode><div class="col full"><input type=text ng-model=vm.search placeholder="ENTER A BOOK, AUTHOR OR ISBN" ng-keyup=vm.searchBook($event)></div></div><div class=results ng-show="vm.editMode && vm.search.length > 0"><div class=splitter></div><h1>SEARCH RESULTS</h1><small>{{vm.searchedBooks.length}} results.</small><div class=search-row><div class="col book" ng-repeat="book in vm.searchedBooks" ng-if=vm.resultIsArray><a ng-href=//www.goodreads.com/book/show/{{book.best_book.id.content}} target=_blank><img ng-src={{book.best_book.image_url}}></a><h2>{{book.best_book.author.name}}</h2><h3>{{book.best_book.title}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.average_rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.average_rating | range)"></i> <span>{{book.ratings_count.content}} voters</span></h2><i class="fa fa-check-circle" ng-show=book.mine></i> <button class=large-button ladda=book.submitLoading ng-click=vm.submit(book) ng-disabled=book.mine>{{book.mine ? \'ADDED\' : \'ADD\'}} TO MY REQUESTS</button></div></div></div></div>'),
        e.put("app/components/mainsearch/mainsearch.html", '<div class=tabs><a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 0" ng-class="{\'active\': vm.selectedTab == 0}">Users <sub>{{vm.searchResults.users.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 1" ng-class="{\'active\': vm.selectedTab == 1}">Books <sub>{{vm.searchResults.books.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 2" ng-class="{\'active\': vm.selectedTab == 2}">Requests <sub>{{vm.searchResults.requests.length}}</sub></a> <a href=javascript:void(0) class=tab ng-click="vm.selectedTab = 3" ng-class="{\'active\': vm.selectedTab == 3}">Goodreads <sub>{{vm.searchResults.goodreads.length}}</sub></a></div><div class=container ng-show="vm.selectedTab == 1"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.books.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.books"><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><strong>£{{book.price}}</strong> <i class="fa fa-check-circle" ng-show=book.mine></i> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitbook(book) ng-disabled="book.user_id == vm.global.user.id">{{book.user_id == vm.global.user.id ? \'ADDED\' : \'ADD\'}} TO MY BOOKS</button> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitrequest(book) ng-disabled="book.user_id == vm.global.user.id">{{book.user_id == vm.global.user.id ? \'ADDED\' : \'ADD\'}} TO MY REQUESTS</button></div></div></div></div><div class=container ng-show="vm.selectedTab == 0"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.users.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.users.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col user" ng-repeat="user in vm.searchResults.users" ng-click=vm.navigateToUser(user)><img class=profile-picture ng-src={{"../"+user.img}} onerror="this.src=\'assets/images/default-user.png\'"><h3>{{user.fullname}}</h3><h2>{{user.city}}</h2><h2><i class="fa fa-star" ng-repeat="i in user.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - user.rating | range)"></i> <span>{{user.ratings_count}} voters</span></h2></div></div></div></div><div class=container ng-show="vm.selectedTab == 2"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.requests.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.requests"><a ng-href=//www.goodreads.com/book/show/{{book.goodreads_id}} target=_blank><img ng-src={{book.image_path}}></a><h2>{{book.author}}</h2><h3>{{book.name}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.rating | range)"></i> <span>{{book.ratings_count}} voters</span></h2><i class="fa fa-check-circle" ng-show=book.mine></i> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitbook(book) ng-disabled="book.user_id == vm.global.user.id">{{book.user_id == vm.global.user.id ? \'ADDED\' : \'ADD\'}} TO MY BOOKS</button> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitrequest(book) ng-disabled="book.user_id == vm.global.user.id">{{book.user_id == vm.global.user.id ? \'ADDED\' : \'ADD\'}} TO MY REQUESTS</button></div></div></div></div><div class=container ng-show="vm.selectedTab == 3"><h1>SEARCH RESULTS.</h1><small>{{vm.searchResults.goodreads.length}} results.</small><div class=results-row ng-show=!vm.editMode ng-class="{\'focus\':vm.commentMode}"><div class=blur ng-show=vm.commentMode ng-click=vm.blur()></div><div class=splitter></div><div class=empty ng-show="vm.searchResults.books.length == 0"><p>NO RESULTS WERE FOUND.</p></div><div class=search-row><div class="col book" ng-repeat="book in vm.searchResults.goodreads"><a ng-href=//www.goodreads.com/book/show/{{book.best_book.id.content}} target=_blank><img ng-src={{book.best_book.image_url}}></a><h2>{{book.best_book.author.name}}</h2><h3>{{book.best_book.title}}</h3><h2><i class="fa fa-star" ng-repeat="i in book.average_rating | range"></i><i class="fa fa-star o" ng-repeat="i in (5 - book.average_rating | range)"></i> <span>{{book.ratings_count.content}} voters</span></h2><i class="fa fa-check-circle" ng-show=book.mine></i> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitbook(book) ng-disabled=book.mine>{{book.mine ? \'ADDED\' : \'ADD\'}} TO MY BOOKS</button> <button class="large-button half" ladda=book.submitLoading ng-click=vm.submitrequest(book) ng-disabled=book.mine>{{book.mine ? \'ADDED\' : \'ADD\'}} TO MY REQUESTS</button></div></div></div></div>'),
        e.put("app/components/mainsettings/imageModal.html", '<div class=modal-body><div class=cropArea><img-crop image=picture result-image=croppedPicture area-min-size=230></img-crop></div></div><div class=modal-footer><button ng-click=cancel() class="btn btn-warning">Cancel</button> <button ng-click=ok() class="btn btn-primary">Save</button></div>'),
        e.put("app/components/mainsettings/mainsettings.html", '<div class=container><h1>Profile Settings</h1><div class=settings-row><div class=col><img ng-src="{{\'../\'+vm.global.user.img.length > 0 ? vm.global.user.img : vm.global.default_img}}" onerror="this.src=\'assets/images/default-user.png\'" alt=""> <button ng-click="vm.global.user.img = \'assets/images/default-user.png\'"><i class="fa fa-trash"></i></button> <button file-button><i class="fa fa-pencil"></i> <input type=file id=fileInput ng-model=vm.picture base-sixty-four-input parser=vm.imgParser></button></div><div class=col><h2 ng-class="{\'error\': vm.global.user.fullname.length == 0}">NAME <span>This field can\'t be empty</span></h2><input type=text ng-model=vm.global.user.fullname></div></div><div class=settings-row><div class=col><h2>Password</h2><input type={{vm.passwordVisible}} ng-model=vm.global.user.password> <a href=javascript:void(0) ng-mousedown="vm.passwordVisible = \'text\'" ng-mouseup="vm.passwordVisible = \'password\'"><i class="fa fa-eye"></i></a></div></div><div class=settings-row><div class=col><h2 ng-class="{\'error\': vm.global.user.location.length == 0}">Location <span>This field can\'t be empty</span></h2><input type=text ng-model=vm.global.user.city></div><div class=settings-row><div class="col full"><h2>Bio</h2><textarea ng-model=vm.global.user.bio></textarea></div></div><button class=large-button ladda=vm.submitLoading ng-click=vm.submit()>Update Account Settings</button></div>'),
        e.put("app/components/navbar/navbar.html", '<header id=header class=clearfix><nav class="navbar navbar-default"><div class=container-fluid><div class=navbar-header><button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target=#main-navbar aria-expanded=false><span class=sr-only>Toggle navigation</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span></button> <a href=javascript:void(0) ui-sref=guestbooks id=logo><span data-hover=BOOKWORM>BOOKWORM</span></a></div><div class="collapse navbar-collapse" id=main-navbar><ul ng-if="!vm.guest" class="nav navbar-nav user-center"><li><a href=javascript:void(0) ng-class="{\'active\': vm.global.user.messages_unseen.length > 0}" ng-click="vm.setNotificationsSeen(); vm.messagesVisible = !vm.messagesVisible;"><i class="fa fa-envelope-o"></i><sub ng-show="vm.global.user.messages_unseen.length > 0">{{vm.global.user.messages_unseen.length}}</sub></a><ul class=notifications ng-class="{\'visible\': vm.messagesVisible}"><li class=arrow></li><li class=header>Messages</li><li class=notification ng-repeat="message in vm.global.user.messages_unseen" ng-class="{\'seen\': message.seen == 1}" ng-click="vm.openMessage(message, $index);"><img ng-src={{"../"+vm.global.users[vm.finduser(message)].img}} onerror="this.src=\'assets/images/default-user.png\'" alt=""><p><strong>{{message.name}}</strong> {{message.message | emojify}}</p><small>{{message.timestamp | relativeDate}}</small></li><li class=empty ng-show="vm.global.user.messages_unseen.length == 0"><p>You have no new messages.</p></li></ul></li><li><a href=javascript:void(0) ng-class="{\'active\': vm.global.user.notifications_unseen > 0}" ng-click="vm.setNotificationsSeen(); vm.notificationsVisible = !vm.notificationsVisible;"><i class="fa fa-bell-o"></i><sub ng-show="vm.global.user.notifications_unseen > 0">{{vm.global.user.notifications_unseen}}</sub></a><ul class=notifications ng-class="{\'visible\': vm.notificationsVisible}"><li class=arrow></li><li class=header>Notifications</li><li class=notification ng-repeat="notification in vm.global.user.notifications" ng-class="{\'seen\': notification.seen == 1}" ng-click="vm.goToNotification(notification); vm.notificationsVisible = false"><img ng-src={{"../"+vm.global.users[vm.finduser(notification)].img}} onerror="this.src=\'assets/images/default-user.png\'"><p><strong>{{notification.name}}</strong> {{notification.notification}}</p><small>{{notification.timestamp | relativeDate}}</small></li><li class=empty ng-show="vm.global.user.notifications_unseen == 0"><p>You have no new notifications.</p></li></ul></li></ul><ul ng-if=vm.guest class="nav yamm navbar-nav navbar-left"><li><a ui-sref=guestbooks><span>Explore</span></a></li></ul><form class=search-container><a href=#><i class="fa fa-search"></i></a> <input class=search placeholder=Search ng-keydown=vm.global.getSearch($event) ng-model=vm.global.keyword></form><ul ng-if="!vm.guest" class="nav yamm navbar-nav navbar-right"><li><a href=javascript:void(0) ui-sref=admin ng-if=vm.global.selectedUser.isAdmin><span>Admin</span> <i class="fa fa-cog"></i></a></li><li><a href=javascript:void(0) ng-click="vm.profileSettings = !vm.profileSettings"><span>{{vm.global.user.fullname}}</span> <i class=fa ng-class="{\'fa-caret-down\':!vm.profileSettings, \'fa-caret-up\':vm.profileSettings}"></i></a><ul ng-show=vm.profileSettings><li><a ng-href=#/users/{{vm.global.user.id}} ng-click="vm.profileSettings = false">Profile</a></li><li><a href=#/settings ng-click="vm.profileSettings = false">Settings</a></li><li><a href=../auth/logout>Logout</a></li></ul></li><li><img ng-src={{"../"+vm.global.user.img}} onerror="this.src=\'assets/images/default-user.png\'"></li></ul><ul ng-if=vm.guest class="nav yamm navbar-nav navbar-right"><li><a href=../auth/login><span>SIGN IN</span></a></li><li><a href=../auth/create_user><span>SIGN UP</span></a></li></ul></div></div></nav></header>')
}]);