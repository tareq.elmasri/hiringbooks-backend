<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Bookworm | Homepage</title>
    <meta charset="utf-8">
    <meta name="description" content="A catchy description">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="E-Commerce, Online, Market, Cloths, T-Shirts">
    <link rel="icon" type="image/png" href="images/logo/logo_32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/logo/logo_16.png" sizes="16x16" />
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" type="image/png" href="images/logo/logo_57.png">
    <!-- iPhone -->
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="images/logo/logo_72.png">
    <!-- iPad -->
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="images/logo/logo_114.png">
    <!-- iPhone4 -->

    <link rel="stylesheet" href="css/vendor.css">

    <link rel="stylesheet" href="css/styles.css">
</head>

<body class="en home">
    <!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <header id="header" class="clearfix">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="javascript:void(0)" id="logo">
                            <span data-hover="BOOKWORM">BOOKWORM</span>
                        </a>
                    </div>

                    <div class="collapse navbar-collapse" id="main-navbar">
                        <ul class="nav yamm navbar-nav navbar-right">
                          <?php if(!$loggedin) { ?>
                            <li><a href="auth/login">LOGIN</a></li>
                            <li><a href="auth/create_user">SIGN UP</a></li>
                          <?php } else { ?>
                            <li><a href="profile">Profile</a></li>
                            <li><a href="messages"><i class="fa fa-envelope-o"></i></a></li>
                            <li><a href="auth/logout">Logout</a></li>
                          <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>


    <div id="body" class="clearfix">
        <div class="container">
            <section class="body-section">
                <h3 class="section-header">
                    FEATURED BOOKS
                </h3>
                <?php for ($i = 0; $i < count($mybooks); $i++): ?>
                  <div class="section-body products">
                      <div class="product col-xs-12 col-md-3 wow fadeInLeft" data-wow-delay="0.<?=$i?>s">
                          <div class="product-inside">
                              <div class="product-header">
                                  <span class="productimage" style="background-image:url('resources/books_images/<?=$mybooks[$i]->image_path;?>')"></span>
                              </div>
                              <div class="product-body">
                                  <h4 class="product-title"><?= $mybooks[$i]->name; ?></h4>
                                  <p><?= $mybooks[$i]->author; ?></p>
                              </div>
                          </div>
                      </div>
                <?php endfor; ?>
                <button class="btn btn-default btn-more">SEE MORE</button>
            </section>
        </div>
    </div>

    <footer id="footer" class="clearfix">
        <div id="inside-footer">
            <div class="container">
                <div class="footer-section col-xs-12 col-md-4">
                    <h4 class="section-header">
                        <span>About</span> Bookworm
                    </h4>
                    <div class="section-body">
                        <p>
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has
                            survived not only five centuries, but also the leap into electronic typesetting.
                        </p>
                        <div class="social">
                            <a href="javascript:void(0)" class="fa fa-facebook"></a>
                            <a href="javascript:void(0)" class="fa fa-twitter"></a>
                            <a href="javascript:void(0)" class="fa fa-google-plus"></a>
                            <a href="javascript:void(0)" class="fa fa-vimeo"></a>
                            <a href="javascript:void(0)" class="fa fa-rss"></a>
                        </div>
                    </div>
                </div>
                <div class="footer-section col-xs-12 col-md-4">
                    <h4 class="section-header">
                        <span>Subscribe</span> to our newsletter
                    </h4>
                    <div class="section-body">
                        <p>
                            Subscribe to our newsletter and join our subscribers.
                        </p>
                        <form class="subscribe" action="#" method="post">
                            <input type="text" class="email" name="email">
                            <button type="button" class="submit" name="button">SUBSCRIBE</button>
                        </form>
                    </div>
                </div>
                <div class="footer-section col-xs-12 col-md-4">
                    <h4 class="section-header">
                    </h4>
                    <div class="section-body">
                    </div>
                </div>
            </div>
        </div>
        <div id="subfooter">
            <div class="container">
                <div id="copyrights" class="col-xs-12 col-sm-4 col-md-3">
                    <p>2015 © All Rights Reserved</p>
                </div>

            </div>
        </div>
    </footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>

    <script src="js/body.js"></script>

    <script src="js/main.js"></script>
</body>

</html>
