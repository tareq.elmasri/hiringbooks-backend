<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Bookworm | Homepage</title>
    <meta charset="utf-8">
    <meta name="description" content="A catchy description">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="keywords" content="E-Commerce, Online, Market, Cloths, T-Shirts">
    <link rel="icon" type="image/png" href="images/logo/logo_32.png" sizes="32x32" />
    <link rel="icon" type="image/png" href="images/logo/logo_16.png" sizes="16x16" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" type="image/png" href="images/logo/logo_57.png">
    <!-- iPhone -->
    <link rel="apple-touch-icon" type="image/png" sizes="72x72" href="images/logo/logo_72.png">
    <!-- iPad -->
    <link rel="apple-touch-icon" type="image/png" sizes="114x114" href="images/logo/logo_114.png">
    <!-- iPhone4 -->

    <link rel="stylesheet" href="css/vendor.css">

    <link rel="stylesheet" href="css/styles.css">
</head>

<body class="en home">
    <!--[if lt IE 10]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <main>
          <div id="slider">
            <div class="slides">
              <img src="images/bg.jpg" alt="" class="slide fixed">
              <img src="images/bg.jpg" alt="" class="slide clear">
            </div>
            <header id="header" class="clearfix">
              <div class="container">
                <nav class="navbar navbar-default">
                  <div class="container-fluid">
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a href="javascript:void(0)" id="logo">
                        <span data-hover="BOOKWORM">BOOKWORM</span>
                      </a>
                    </div>

                    <div class="collapse navbar-collapse" id="main-navbar">
                      <ul class="nav yamm navbar-nav navbar-right">
                       <?php if(!$loggedin) { ?>
                         <li><a href="auth/login">SIGN IN</a></li>
                         <li><a href="auth/create_user">SIGN UP</a></li>
                       <?php } else { ?>
                         <li><a href="dashboard">PROFILE</a></li>
                         <li><a href="auth/logout">SIGN OUT</a></li>
                       <?php } ?>
                      </ul>
                    </div>
                  </div>
                </nav>
              </div>
            </header>

            <div class="content">
              <div class="container">
                <div class="col-xs-12">
                  <div class="form-group">
                    <h2 class="slide-title">Best source for used books.</h2>
                    <form onsubmit="window.location.href = 'dashboard/#/guestsearch?keyword=' + encodeURI($(this).find('input').val()); return false" class="search-container">
                      <input type="text" name="keyword" class="form-control" placeholder="SEARCH">
                      <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </main>

    <div id="body" class="clearfix">
        <div class="container">
          <section class="body-section">
            <h3 class="section-header">HOW IT WORKS</h3>
            <div class="section-body howitworks">
              <div class="instruction wow fadeInDown col-xs-12 col-md-4" data-wow-delay="0.1s">
                <span style="background-image:url(images/how_a.png)"></span>
                <h3>Register</h3>
                <p>Lorem ipsum dolor sit amet.<br>consectetur adispisicinge elit.</p>
              </div>
              <div class="instruction wow fadeInDown col-xs-12 col-md-4" data-wow-delay="0.2s">
                <span style="background-image:url(images/how_b.png)"></span>
                <h3>Find a book</h3>
                <p>Lorem ipsum dolor sit amet.<br>consectetur adispisicinge elit.</p>
              </div>
              <div class="instruction wow fadeInDown col-xs-12 col-md-4" data-wow-delay="0.3s">
                <span style="background-image:url(images/how_c.png)"></span>
                <h3>Make a request</h3>
                <p>Lorem ipsum dolor sit amet.<br>consectetur adispisicinge elit.</p>
              </div>
            </div>
          </section>
            <section class="body-section">
                <h3 class="section-header">
                    FEATURED BOOKS
                </h3>
                <div class="books-row">
                <?php for ($i = 0; $i < count($books); ++$i): ?>
                 <div class="col book">
                   <div class="body">
                     <a ng-href="//www.goodreads.com/book/show/<?= $books[$i]->goodreads_id; ?>" target="_blank"><img src="<?= $books[$i]->image_path; ?>"></a>
                     <h2><?= $books[$i]->author; ?></h2>
                     <h3><?= $books[$i]->name; ?></h3>
                     <h2>
                       <?php for ($k=0; $k < 5 - $books[$i]->rating; $k++){ ?>
                         <i class="fa fa-star"></i>
                       <?php } ?>
                       <?php for ($j=0; $j < $books[$i]->rating; $j++){ ?>
                         <i class="fa fa-star o"></i>
                       <?php } ?>
                       <span><?= $books[$i]->ratings_count; ?> voters</span>
                     </h2>
                     <strong>£<?= $books[$i]->price; ?></strong>
                   </div>
                 </div>
                <?php endfor; ?>
                <a href="dashboard/#/guestbooks" class="btn btn-default btn-more">SEE MORE</a>
               </div>
            </section>
            <section class="body-section">
                <h3 class="section-header">
                    Requests
                </h3>
                <div class="books-row">
                <?php for ($i = 0; $i < count($requests); ++$i): ?>
                 <div class="col book">
                   <div class="body">
                     <a ng-href="//www.goodreads.com/book/show/<?= $requests[$i]->goodreads_id; ?>" target="_blank"><img src="<?= $requests[$i]->image_path; ?>"></a>
                     <h2><?= $requests[$i]->author; ?></h2>
                     <h3><?= $requests[$i]->name; ?></h3>
                     <h2>
                       <?php for ($k=0; $k < 5 - $requests[$i]->rating; $k++){ ?>
                         <i class="fa fa-star"></i>
                       <?php } ?>
                       <?php for ($j=0; $j < $requests[$i]->rating; $j++){ ?>
                         <i class="fa fa-star o"></i>
                       <?php } ?>
                       <span><?= $requests[$i]->ratings_count; ?> voters</span>
                     </h2>
                   </div>
                 </div>
                <?php endfor; ?>
                <a href="dashboard/#/guestbooks" class="btn btn-default btn-more">SEE MORE</a>
               </div>
            </section>
        </div>
    </div>

    <footer id="footer" class="clearfix">
      <div id="inside-footer">
        <div class="container">
          <div class="footer-section col-xs-12">
            <div class="section-body text-center">
              <p>
                All copyrights © reserved, BOOKWORM. 2016
              </p>
              <div class="footer-links">
                <a href="policy">Privacy Policy</a>
                <a href="javascript:void(0)">Terms of use</a>
                <a href="javascript:void(0)">Help</a>
                <a href="javascript:void(0)">Contact</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
    (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
    function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
    e=o.createElement(i);r=o.getElementsByTagName(i)[0];
    e.src='//www.google-analytics.com/analytics.js';
    r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
    ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>

    <script src="js/body.js"></script>

    <script src="js/main.js"></script>
</body>

</html>
