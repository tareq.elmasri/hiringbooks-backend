<div class="wrapper">
		<h1>Sign up</h1>
		<img src="../images/logo.png" class="logo" alt="" />

	<div id="infoMessage"><?php echo $message;?></div>

    <?php echo form_open("auth/create_user");?>
      <div class="form-group">
      <?php echo form_input($fullname,"","class='form-control' placeholder='Full Name'");?>
		</div>

      <div class="form-group">
      <?php echo form_input($username,"","class='form-control' placeholder='Username'");?>
		</div>

      <div class="form-group">
      <?php echo form_input($email,"","class='form-control' placeholder='Email'");?>
		</div>

		<div class="form-group">
			<?php echo form_dropdown($gender,[0=>"Male", 1=>"Female"], [0], "class='form-control' placeholder='Gender'");?>
		</div>
      <div class="form-group">
      <?php echo form_input($city,"","class='form-control' placeholder='City'");?>
		</div>

      <div class="form-group">
      <?php echo form_input($password,"","class='form-control' placeholder='Password'");?>
		</div>

      <div class="form-group">
      <?php echo form_input($password_confirm,"","class='form-control' placeholder='Confirm Password'");?>
		</div>


      <div class="form-group"><?php echo form_submit('submit', 'Create User', "class='btn btn-default'");?></div>

    <?php echo form_close();?>

</div>
