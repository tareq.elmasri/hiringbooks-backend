<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Search extends REST_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('search_model');
  }

  public function index_get() {
  {
    $keyword = $this->get('keyword');
    // If the id parameter doesn't exist return all the users
    if ($keyword !== NULL) {
        // Check if the users data store contains users (in case the database result returns NULL)
        if (!empty($this->search_model->getSearchByKeyword($keyword))) {
            // Set the response and exit
            $this->response($this->search_model->getSearchByKeyword($keyword), REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No search results were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No data were sent'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }
 }
}