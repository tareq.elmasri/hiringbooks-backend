<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Requests extends REST_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->model('requests_model');
    $this->load->helper('file');
    $this->load->database();
  }

  public function index_get() {
    $id = $this->get('id');
    // If the id parameter doesn't exist return all the users
    if ($id !== NULL) {
        // Check if the users data store contains users (in case the database result returns NULL)
        if (!empty($this->requests_model->get_requests_byuserid($id))) {
            // Set the response and exit
            $this->response([
              'requests' => $this->requests_model->get_requests_byuserid($id),
              'comments' => $this->requests_model->get_comments_byuserid($id)
            ]);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No requests were found'
            ]);
        }
    } else {
      $this->response($this->requests_model->get_allrequests());
    }
  }

  public function index_post() {
    $request = $this->request->body;
    $data = array(
      'name' => $request['best_book']['title'],
      'author' => $request['best_book']['author']['name'],
      'user_id' => $request['user_id'],
      'fullname' => $request['fullname'],
      'image_path' => $request['best_book']['image_url'],
      'rating' => round($request['average_rating']),
      'ratings_count' => $request['ratings_count']['content'],
      'goodreads_id' => $request['best_book']['id']['content'],
      'status' => 1,
      'timestamp' => $request['timestamp']
    );
    if ($this->requests_model->post_request($data)) {
        $this->response($this->requests_model->get_request_bygoodreadsid($data['goodreads_id']), REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function comment_request_post() {
    $data = $this->request->body;
    // If the id parameter doesn't exist return all the users
    if ($this->requests_model->post_comment($data)) {
      $this->response($data, REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function status_request_post() {
    $data = $this->request->body;
    // If the id parameter doesn't exist return all the users
    if ($this->requests_model->status_request($data)) {
      $this->response([
          'status' => true,
          'message' => 'Request deleted successfully.'
      ], REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function delete_request_post() {
    $id = $this->post('id');
    // If the id parameter doesn't exist return all the users
    if ($id !== NULL) {
        // Check if the users data store contains users (in case the database result returns NULL)
        if (!empty($this->requests_model->delete_request($id))) {
            // Set the response and exit
            $this->response([
                'status' => true,
                'message' => 'Request updated successfully.'
            ], REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No requests were found'
            ], REST_Controller::HTTP_NO_CONTENT);
        }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No id were sent'
      ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }
}
