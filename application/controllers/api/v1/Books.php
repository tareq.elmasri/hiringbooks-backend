<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Books extends REST_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->model('books_model');
    $this->load->helper('file');
    $this->load->database();
  }

  public function index_get() {
    $id = $this->get('id');
    // If the id parameter doesn't exist return all the users
    if ($id !== NULL) {
        // Check if the users data store contains users (in case the database result returns NULL)
        if (!empty($this->books_model->get_books_byuserid($id))) {
            // Set the response and exit
            $this->response([
              'books' => $this->books_model->get_books_byuserid($id),
              'comments' => $this->books_model->get_comments_byuserid($id)
            ]);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No books were found'
            ]);
        }
    } else {
      $this->response($this->books_model->get_allbooks());
    }
  }

  public function index_post() {
    $request = $this->request->body;
    $data = array(
      'name' => $request['best_book']['title'],
      'fullname' => $request['fullname'],
      'author' => $request['best_book']['author']['name'],
      'user_id' => $request['user_id'],
      'image_path' => $request['best_book']['image_url'],
      'rating' => round($request['average_rating']),
      'ratings_count' => $request['ratings_count']['content'],
      'goodreads_id' => $request['best_book']['id']['content'],
      'status' => 1,
      'price' => $request['price'] ? $request['price'] : 0,
      'timestamp' => $request['timestamp']
    );
    if ($this->books_model->post_book($data)) {
        $this->response($this->books_model->get_book_bygoodreadsid($data['goodreads_id']), REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function comment_book_post() {
    $data = $this->request->body;
    // If the id parameter doesn't exist return all the users
    if ($this->books_model->post_comment($data)) {
      $this->response($data, REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function status_book_post() {
    $data = $this->request->body;
    if ($this->books_model->status_book($data)) {
      $this->response([
          'status' => true,
          'message' => 'Book deleted successfully.'
      ], REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function update_price_post() {
    $data = $this->request->body;
    if ($this->books_model->price_book($data)) {
      $this->response([
          'status' => true,
          'message' => 'Book updated successfully.'
      ], REST_Controller::HTTP_OK);
    } else {
        $this->response([
            'status' => FALSE,
            'message' => 'Update has been failed'
        ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function delete_book_post() {
    $id = $this->post('id');
    // If the id parameter doesn't exist return all the users
    if ($id !== NULL) {
        // Check if the users data store contains users (in case the database result returns NULL)
        if (!empty($this->books_model->delete_book($id))) {
            // Set the response and exit
            $this->response([
                'status' => true,
                'message' => 'Book updated successfully.'
            ], REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No books were found'
            ], REST_Controller::HTTP_NO_CONTENT);
        }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No id were sent'
      ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }
}
