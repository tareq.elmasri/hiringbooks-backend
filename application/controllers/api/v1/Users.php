<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Users extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('file');
        $this->load->library('session');
        $this->load->library('ion_auth');
        $this->load->model('wall_model');
        $this->load->model('ratings_model');
        $this->load->model('messages_model');
        $this->load->model('ratings_model');
        $this->load->model('activity_model');
        $this->load->model('notifications_model');
        $this->load->database();
    }

    public function index_get()
    {
        $id = $this->get('id');
        if ($id !== null) {
            if (!empty($this->ion_auth->users($id))) {
                $user = $this->ion_auth->users($id)->row();
                unset($user->password);
                $this->response($this->ion_auth->users($id)->row(), REST_Controller::HTTP_OK);
            } else {
                $this->response([
                  'status' => false,
                  'message' => 'No users were found',
                ], REST_Controller::HTTP_NOT_FOUND);
            }
        } else {
            $users = $this->ion_auth->users()->result();
            foreach ($users as $user) {
                unset($user->fb_id);
                unset($user->password);
                unset($user->activation_code);
                unset($user->forgotten_password_code);
                unset($user->remember_code);
                unset($user->ip_address);
                unset($user->salt);
                $user->wall = $this->wall_model->get_byuserid($user->id);
                $user->wall_comments = $this->wall_model->get_comments_byuserid($user->id);
                $user->activity = $this->activity_model->get_activity($user->id);
                $user->isAdmin = $this->ion_auth->in_group("admin", $user->id);
            }
            $this->response($users);
        }
    }

    public function current_user_get()
    {
        $current_user = $this->ion_auth_model->user()->row();
        $isAdmin = ['isAdmin' => $this->ion_auth->is_admin()];
        if (!empty($current_user)) {
            $notifications = ['notifications' => $this->notifications_model->get_byuserid($current_user->id)];
            $messages = ['messages' => $this->messages_model->get_byuserid($current_user->id)];
            $activity = ['activity' => $this->activity_model->get_activity($current_user->id)];
            $wall = ['wall' => $this->wall_model->get_byuserid($current_user->id)];
            $wall_comments = ['wall_comments' => $this->wall_model->get_comments_byuserid($current_user->id)];
            $current_user = (object) array_merge((array) $current_user, (array) $notifications, (array) $messages, (array) $isAdmin, (array) $activity, (array) $wall, (array) $wall_comments);
            unset($current_user->password);
            $this->response($current_user);
        } else {
            $this->response([
          'status' => false,
          'message' => 'You have to login.',
      ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function index_post()
    {
        $data = $this->request->body;
        $id = $data['id'];
        if ($this->request->body !== null) {
            $decodedImage = base64_decode($data['img']);
            $data['img'] = "resources/users_images/$id-".date_timestamp_get(date_create()).'.jpg';
            unset($data['id']);
            if (write_file(FCPATH.'/'.$data['img'], $decodedImage)
            && $this->ion_auth_model->update($id, $data)) {
                $this->response($this->ion_auth_model->user()->row(), REST_Controller::HTTP_OK);
            } else {
                $this->response([
            'status' => false,
            'message' => 'Update has been failed',
        ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response([
          'status' => false,
          'message' => 'No data were sent',
      ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function rate_post()
    {
       $data = $this->request->body;
       $id = $data['target_user_id'];
       if ($this->request->body !== NULL) {
         $this->ratings_model->post_rating();
       } else {
           $this->response([
         'status' => false,
         'message' => 'No data were sent',
     ], REST_Controller::HTTP_BAD_REQUEST);
       }
    }

    public function delete_user_post()
    {
       $data = $this->request->body;
       $id = $data['id'];
       if ($this->request->body !== NULL) {
         $this->ion_auth_model->delete_user($id);
         $this->wall_model->delete_user($id);
         $this->activity_model->delete_user($id);
         $this->notifications_model->delete_user($id);
         $this->books_model->delete_user($id);
         $this->requests_model->delete_user($id);
         $this->response([
          'status' => true,
          'message' => 'User deleted successfully.',
         ], REST_Controller::HTTP_OK);
       } else {
           $this->response([
         'status' => false,
         'message' => 'No data were sent',
     ], REST_Controller::HTTP_BAD_REQUEST);
       }
    }

    public function suspend_user_post()
    {
       $data = $this->request->body;
       $id = $data['id'];
       if ($this->request->body !== NULL) {
         $this->ion_auth_model->update($id, array('suspended' => $data['suspended']));
         $this->response([
          'status' => true,
          'message' => 'User suspended successfully.',
         ], REST_Controller::HTTP_OK);
       } else {
           $this->response([
         'status' => false,
         'message' => 'No data were sent',
     ], REST_Controller::HTTP_BAD_REQUEST);
       }
    }

    public function rate_user_post()
    {
       $rating_data = $this->request->body;
       $id = $rating_data['target_user_id'];
       if ($this->request->body !== NULL) {
         $this->ratings_model->post_rating($rating_data);
         $query = $this->db->get_where('users', array('id' => $id));
         $this->response($query->row_array(), REST_Controller::HTTP_OK);
       } else {
           $this->response([
         'status' => false,
         'message' => 'No data were sent',
     ], REST_Controller::HTTP_BAD_REQUEST);
       }
    }
}
