<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Reports extends REST_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->model('reports_model');
    $this->load->database();
  }

  public function index_get() {
    $user_id = $this->get('user_id');
    $target_id = $this->get('target_id');
    if ($user_id !== NULL && $target_id !== NULL) {
        if (!empty($this->reports_model->get_reports($user_id, $target_id))) {
            // Set the response and exit
            $this->response($this->reports_model->get_reports($user_id, $target_id), REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No messages were found'
            ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    } else {
      $this->response($this->reports_model->get_reports(null, null), REST_Controller::HTTP_OK);
    }
  }

  public function index_post() {
    $data = $this->request->body;
    if ($this->request->body !== NULL) {
      if ($this->reports_model->set_report($data)) {
        $this->response([
         'status' => TRUE,
         'message' => 'Report submitted successfully.'
        ], REST_Controller::HTTP_OK);
      } else {
          $this->response([
              'status' => FALSE,
              'message' => 'Update has been failed'
          ], REST_Controller::HTTP_BAD_REQUEST);
      }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No data were sent'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function set_seen_post() {
    $data = $this->request->body;
    if (!empty($data) && $data['user_id'] !== $data['target_user_id']) {
      if ($this->reports_model->set_seen($data['user_id'], $data['target_user_id'])) {
          $this->response($data, REST_Controller::HTTP_OK);
      } else {
          $this->response([
              'status' => FALSE,
              'message' => 'Update has been failed'
          ], REST_Controller::HTTP_BAD_REQUEST);
      }
    } else {
      $this->reports_model->set_seen();
      $this->response([
       'status' => TRUE,
       'message' => 'Updated successfully'
      ], REST_Controller::HTTP_OK);
    }
  }
}
