<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Messages extends REST_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('session');
    $this->load->library('ion_auth');
    $this->load->model('messages_model');
    $this->load->database();
  }

  public function index_get() {
    $user_id = $this->get('user_id');
    $target_id = $this->get('target_id');
    $offset = $this->get('offset');
    if ($user_id !== NULL && $target_id !== NULL) {
        if (!empty($this->messages_model->get_messages($user_id, $target_id, $offset))) {
            // Set the response and exit
            $this->response($this->messages_model->get_messages($user_id, $target_id, $offset), REST_Controller::HTTP_OK);
        }
        else {
            // Set the response and exit
            $this->response([
                'status' => FALSE,
                'message' => 'No messages were found'
            ], REST_Controller::HTTP_NO_CONTENT); // NOT_FOUND (404) being the HTTP response code
        }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No messages were found'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function index_post() {
    $data = $this->request->body;
    if ($this->request->body !== NULL) {
      if ($this->messages_model->set_message($data)) {
          $this->response($data, REST_Controller::HTTP_OK);
      } else {
          $this->response([
              'status' => FALSE,
              'message' => 'Update has been failed'
          ], REST_Controller::HTTP_BAD_REQUEST);
      }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No data were sent'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }

  public function set_seen_post() {
    $data = $this->request->body;
    if (!empty($data)) {
      if ($this->messages_model->set_seen($data['user_id'], $data['target_id'])) {
          $this->response($data, REST_Controller::HTTP_OK);
      } else {
          $this->response([
              'status' => FALSE,
              'message' => 'Update has been failed'
          ], REST_Controller::HTTP_BAD_REQUEST);
      }
    } else {
      $this->response([
          'status' => FALSE,
          'message' => 'No data were sent'
      ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
    }
  }
}
