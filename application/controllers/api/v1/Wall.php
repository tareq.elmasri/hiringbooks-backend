<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Wall extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('wall_model');
        $this->load->database();
    }

    public function index_get()
    {
        $user_id = $this->get('user_id');
        if ($user_id !== null) {
            if (!empty($this->wall_model->get_byuserid($user_id))) {
                $this->response($this->wall_model->get_byuserid($user_id), REST_Controller::HTTP_OK);
            } else {
                $this->response([
                     'status' => false,
                     'message' => 'No notifications were found',
                  ], REST_Controller::HTTP_NO_CONTENT
               );
            }
        } else {
            $this->response([
               'status' => false,
               'message' => 'No notifications were found',
            ], REST_Controller::HTTP_BAD_REQUEST
         );
        }
    }

    public function index_post()
    {
        $data = $this->request->body;
        if ($this->request->body !== null) {
            if ($this->wall_model->set_wall($data)) {
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                   'status' => false,
                   'message' => 'Post has been failed',
                ], REST_Controller::HTTP_BAD_REQUEST
             );
            }
        } else {
            $this->response([
               'status' => false,
               'message' => 'No data were sent',
            ], REST_Controller::HTTP_BAD_REQUEST
         );
        }
    }

    public function comment_post_post()
    {
        $data = $this->request->body;
        if ($this->wall_model->post_comment($data)) {
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response([
                 'status' => false,
                 'message' => 'Update has been failed',
              ], REST_Controller::HTTP_BAD_REQUEST
           );
        }
    }

    public function delete_post_post() {
      $id = $this->post('id');
      // If the id parameter doesn't exist return all the users
      if ($id !== NULL) {
          // Check if the users data store contains users (in case the database result returns NULL)
          if (!empty($this->wall_model->delete_book($id))) {
              // Set the response and exit
              $this->response([
                  'status' => true,
                  'message' => 'Post updated successfully.'
              ], REST_Controller::HTTP_OK);
          }
          else {
              // Set the response and exit
              $this->response([
                  'status' => FALSE,
                  'message' => 'No posts were found'
              ], REST_Controller::HTTP_NO_CONTENT);
          }
      } else {
        $this->response([
            'status' => FALSE,
            'message' => 'No id were sent'
        ], REST_Controller::HTTP_BAD_REQUEST);
      }
    }

    public function set_seen_post()
    {
        $data = $this->request->body;
        if ($this->request->body !== null) {
            if ($this->wall_model->set_seen($data['id'])) {
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response([
                   'status' => false,
                   'message' => 'Notification update has been failed',
                ], REST_Controller::HTTP_BAD_REQUEST
             );
            }
        } else {
            $this->response([
               'status' => false,
               'message' => 'No data were sent',
            ], REST_Controller::HTTP_BAD_REQUEST
         );
        }
    }
}
