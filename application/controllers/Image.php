<?php

defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Image extends REST_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index_get()
    {
      $imageurl = explode("/", $this->get('img'));
      $url = str_replace(end($imageurl),"", $this->get('img'));
      $img = end($imageurl);
      // print_r($img);
      $image = file_get_contents($url.$img);
      header("Content-type: image/jpeg");
      echo $image;
    }
   }