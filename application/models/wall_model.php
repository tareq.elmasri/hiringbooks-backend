<?php

defined('BASEPATH') or exit('No direct script access allowed');

class wall_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('notifications_model');
    }

    public function get_byuserid($id)
    {
        if ($id != null) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('wall', array('target_user_id' => $id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function set_wall($data)
    {
        if (!empty($data)) {
            $query = $this->db->insert_string('wall', $data);
            $this->db->query($query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function set_seen($user_id)
    {
        if (!empty($data)) {
            $this->db->order_by('id', 'desc');
            $getMessage = $this->db->get_where('wall', array('target_user_id' => $user_id));
            $result = $getMessage->result();
            foreach ($result as $wall_post) {
                $id = $wall_post->id;
                $query = $this->db->update_string('wall', array('seen' => true), "id = $id");
                $this->db->query($query);
            }

            return true;
        } else {
            return false;
        }
    }
    public function get_comments_byuserid($id)
    {
        if ($id != false) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('comments', array('topic_type' => 'wall', 'target_user_id' => $id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function post_comment($data)
    {
        if (!empty($data)) {
            $id = $data['topic_id'];
            $update_data = array('comments_count' => $data['comments_count']);
            $update_query = $this->db->update_string('wall', $update_data, "id = $id");
            $this->db->query($update_query);
            unset($data['comments_count']);
            if ($data['user_id'] != $data['target_user_id']) {
             $notification_data = array(
               'name' => $data['fullname'],
               'notification' => 'has posted on your wall.',
               'user_id' => $data['user_id'],
               'target_user_id' => $data['target_user_id'],
               'seen' => false,
               'topic_id' => $data['topic_id'],
               'topic_type' => 'books',
               'timestamp' => $data['timestamp'],
             );
             $this->notifications_model->post_notification($notification_data);
            }
            unset($data['fullname']);
            $create_query = $this->db->insert_string('comments', $data);
            $this->db->query($create_query);
            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function delete_book($id)
    {
        if ($id != false) {
            $this->db->delete('wall', array('id' => $id));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function delete_user($id)
    {
        if ($id != false) {
            $this->db->delete('wall', array('target_user_id' => $id));

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }
}
