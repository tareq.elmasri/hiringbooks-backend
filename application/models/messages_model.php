<?php

class messages_model extends CI_Model
{
    public function __construct()
    {
        $this->load->database();
    }

    public function get_messages($user_id, $target_id, $offset)
    {
        if ($user_id != null) {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('messages', array('user_id' => $user_id, 'target_id' => $target_id), 6, $offset);

            return $query->result();
        } else {
            return false;
        }
    }

    public function get_byuserid($user_id)
    {
        if ($user_id != null) {
            $query = $this->db->get_where('messages', array('user_id' => $user_id));

            return $query->result();
        } else {
            return false;
        }
    }

    public function set_message($message)
    {
        if (!empty($message)) {
            $query = $this->db->insert_string('messages', $message);
            $this->db->query($query);

            return $this->db->affected_rows();
        } else {
            return false;
        }
    }

    public function set_seen($user_id, $target_id)
    {
        if (!empty($user_id)) {
            $this->db->order_by('id', 'desc');
            $getMessage = $this->db->get_where('messages', array('user_id' => $user_id, 'target_id' => $target_id));
            $result = $getMessage->result();
            foreach ($result as $message) {
                $id = $message->id;
                $query = $this->db->update_string('messages', array('seen' => true), "id = $id");
                $this->db->query($query);
            }

            return true;
        } else {
            return false;
        }
    }
}
