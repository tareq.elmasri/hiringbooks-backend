<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class search_model extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function getUsers($keyword) {
    $this->db->like('fullname', $keyword);
    $this->db->or_like('email', $keyword);
    $this->db->or_like('college', $keyword);
    $query = $this->db->get('users');
    return $query->result();
  }

  public function getBooks($keyword) {
   $this->db->like('name', $keyword);
   $this->db->or_like('author', $keyword);
   $this->db->or_like('isbn', $keyword);
   $query = $this->db->get('books');
   return $query->result();
  }

  public function getRequests($keyword) {
   $this->db->like('name', $keyword);
   $this->db->or_like('author', $keyword);
   $this->db->or_like('isbn', $keyword);
   $query = $this->db->get('requests');
   return $query->result();
  }

  public function getSearchByKeyword($keyword) {
    return array(
      'users' => $this->getUsers($keyword),
      'books' => $this->getBooks($keyword),
      'requests' => $this->getRequests($keyword)
    );
  }
}
