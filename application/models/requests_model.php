<?php
class requests_model extends CI_Model {

        public function __construct()
        {
          $this->load->database();
          $this->load->model('activity_model');
          $this->load->model('notifications_model');
        }

        public function get_request($id) {
          if($id != FALSE) {
            $query = $this->db->get_where('requests', array('id' => $id));
            return $query->row_array();
          }
          else {
            return FALSE;
          }
        }

        public function get_request_bygoodreadsid($id) {
          if($id != FALSE) {
            $query = $this->db->get_where('requests', array('goodreads_id' => $id));
            return $query->row_array();
          }
          else {
            return FALSE;
          }
        }

        public function get_book_bygoodreadsid($id) {
          if($id != FALSE) {
            $query = $this->db->get_where('books', array('goodreads_id' => $id));
            return $query->row_array();
          }
          else {
            return FALSE;
          }
        }

        public function get_limited_requests()
        {
            $this->db->order_by('id', 'desc');
            $query = $this->db->get_where('requests', null, 4, 0);
            return $query->result();
        }

        public function get_allrequests() {
            $this->db->order_by("id", "desc");
            $query = $this->db->get_where('requests');
            return $query->result();
        }

        public function get_requests_byuserid($id) {
          if($id != FALSE) {
            $this->db->order_by("id", "desc");
            $query = $this->db->get_where('requests', array('user_id' => $id));
            return $query->result();
          }
          else {
            return FALSE;
          }
        }

        public function get_comments_byuserid($id) {
          if($id != FALSE) {
            $this->db->order_by("id", "desc");
            $query = $this->db->get_where('comments', array('topic_type' => 'request', 'target_user_id' => $id));
            return $query->result();
          }
          else {
            return FALSE;
          }
        }

        public function post_comment($data) {
          if(!empty($data)) {
            $id = $data['topic_id'];
            $update_data = array("comments_count"=> $data['comments_count']);
            $update_query = $this->db->update_string('requests', $update_data, "id = $id");
            $this->db->query($update_query);
            unset($data['comments_count']);
            $notification_data = array(
              "timestamp"=> $data['timestamp'],
              "notification" => 'commented on your request.',
              "user_id" => $data['user_id'],
              "target_user_id" => $data['target_user_id'],
              "seen" => FALSE,
              "name" => $data['fullname'],
              "topic_id" => $data['topic_id'],
              "topic_type" => "requests"
            );
            $this->notifications_model->post_notification($notification_data);
            unset($data['fullname']);
            $create_query = $this->db->insert_string('comments', $data);
            $this->db->query($create_query);
            return $this->db->affected_rows();
          }
          else {
            return FALSE;
          }
        }

        public function post_request($data) {
          if(!empty($data)) {
            if ($this->get_book_bygoodreadsid($data['goodreads_id'])) {
              $results = $this->get_book_bygoodreadsid($data['goodreads_id']);
              foreach ($results as $b_book) {
                $notification_data = array(
                  "timestamp"=> $data['timestamp'],
                  "notification" => 'requested a book you have.',
                  "user_id" => $data['user_id'],
                  "target_user_id" => $b_book->user_id,
                  "seen" => FALSE,
                  "name" => $data['fullname'],
                  "topic_id" => $b_book->id,
                  "topic_type" => "books"
                );
                $this->notifications_model->post_notification($notification_data);
              }
            }

            $activity_data = array(
             'timestamp' => $data['timestamp'],
             'user_name' => $data['fullname'],
             'activity' => "has requested a book.",
             'user_id' => $data['user_id'],
             'seen' => false,
             'topic_type' => 'requests',
            );
            unset($data['fullname']);
            $query = $this->db->insert_string('requests', $data);
            $this->db->query($query);
            $last_inserted_book = $this->get_request_bygoodreadsid($data['goodreads_id']);
            $activity_data['topic_id'] = $last_inserted_book['id'];
            $this->activity_model->set_activity($activity_data);
            return $this->db->affected_rows();
          }
          else {
            return FALSE;
          }
        }

        public function delete_request($id) {
          if($id != FALSE) {
            $this->db->delete('requests', array('id' => $id));
            return $this->db->affected_rows();
          }
          else {
            return FALSE;
          }
        }

        public function status_request($body) {
          $id = $body['id'];
          if($id != FALSE) {
            $data = array("status"=> $body['status']);
            $query = $this->db->update_string('requests', $data, "id = $id");
            $this->db->query($query);
            return $this->db->affected_rows();
          }
          else {
            return FALSE;
          }
        }
        public function delete_user($id)
        {
            if ($id != false) {
                $this->db->delete('requests', array('target_user_id' => $id));

                return $this->db->affected_rows();
            } else {
                return false;
            }
        }
}
