<?php
class books_model extends CI_Model {

  public function __construct()
  {
    $this->load->database();
    $this->load->library('email');
  }

  public function send_email($data, $user_id) {
    if(!empty($data)) {
      $this->email->from($data['from_email'], $data['fullname']);
      $this->email->to($data['to_email']);
      $this->email->subject($data['subject']);
      $this->email->message($data['message']);
      $this->email->send();
      return $this->email->print_debugger();
    }
    else {
      return FALSE;
    }
  }

}
